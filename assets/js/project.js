/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for the Assay admin template
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//expense stats
	var expenseStats = new Chart( jQuery('#expenseStats'), {
		type: 'line',
		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
			datasets: [
				{
					data: [0, 200, 100, 300, 200, 380, 180],
					borderColor: "#1dc5e9",
					lineTension: 0.4,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.5)'
				},
				{
					data: [0, 120, 250, 90, 250, 130, 150],
					borderColor: "#a389D4",
					lineTension: 0.5,
					backgroundColor: 'rgba(163, 137, 212, 0.5)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#a389D4',
					radius: 5,
					borderWidth: 2
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//project progress horizontal bar
	var projectProgress = new Chart( jQuery('#projectProgress'), {
		type: 'horizontalBar',
		data: {
			labels: ["Food - IT", "School Website", "Payment App"],
			datasets: [
				{
					data: [60, 50, 40],
					backgroundColor: '#1dc5e9'
				},
				{
					data: [30, 40, 20],
					backgroundColor: '#a389D4'
				},
				{
					data: [20, 50, 30],
					backgroundColor: '#f5c22b'
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:true,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					},
					stacked: true,
					}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:false
					},
					barPercentage: 0.6,
					categoryPercentage: 0.3,
					ticks: {
						display: true,
						beginAtZero:true,
					},
					stacked: true
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//task status pie chart
	var taskStatusPie = new Chart( jQuery('#taskStatusPie'), {
		type: 'doughnut',
		data: {
			labels: ["WIP", "Completed", "Not Started"],
			datasets: [
				{
					data: [
						5,
						9,
						3
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 80
		}
	});
	
	//Connection stats
	var connectionStats = new Chart( jQuery('#connectionStats'), {
		type: 'bar',
		data: {
			labels: ["J", "F", "M", "A", "M", "J", "J", "A", "S"],
			datasets: [
				{
					data: [12, 10, 13, 15, 13, 10, 12, 9, 10],
					backgroundColor: '#a389D4'
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					},
					barPercentage: 0.7,
					categoryPercentage: 0.5,
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display: false
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: false,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//leads stats
	var leadsStats = new Chart( jQuery('#leadsStats'), {
		type: 'line',
		data: {
			labels: ["M", "T", "W", "T", "F"],
			datasets: [
				{
					data: [2, 11, 9, 12, 8],
					borderColor: "#1dc5e9",
					lineTension: 0,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'false',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:true,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:false
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: false,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
});
