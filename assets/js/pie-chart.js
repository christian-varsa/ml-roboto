/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for line charts
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//label
	var label = ["Google", "Bing", "Yahoo", "Others"];
	
	//dataset
	var dataset = [
				{
					data: [
						40,
						20,
						30,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			];
	var datasetOne = [
						{
							backgroundColor : "rgba(245, 194, 43, 0.5)",
							fill: 'end',
							data : [40, 40, 30, 50],
							borderColor: '#f5c22b',
							borderWidth: 2
						},
						{
							backgroundColor : 'rgba(163, 137, 212, 0.5)',
							//fill: -1,
							data : [50, 30, 20, 40],
							borderColor: '#a389D4',
							borderWidth: 2
						},
						{
							backgroundColor : 'rgba(29, 197, 233, 0.5)',
							//fill: -1,
							data : [30, 60, 40, 20],
							borderColor: '#1dc5e9',
							borderWidth: 2
						},
					];
	var resourceUsageDonut = new Chart( jQuery('#resourceUsageDonut'), {
		type: 'doughnut',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 70
		}
	});
	
	var resourceUsagePie = new Chart( jQuery('#resourceUsagePie'), {
		type: 'pie',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			legend: {
				display: false
			},
			//cutoutPercentage: 70
		}
	});
	
	var resourceUsagePolar = new Chart( jQuery('#resourceUsagePolar'), {
		type: 'polarArea',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			legend: {
				display: false
			},
			//cutoutPercentage: 70
		}
	});
	
	var resourceUsageRadar = new Chart( jQuery('#resourceUsageRadar'), {
		type: 'radar',
		data: {
			labels: label,
			datasets: datasetOne
		},
		options: {
			legend: {
				display: false
			},
			//cutoutPercentage: 70
		}
	});
	
	//END
});
