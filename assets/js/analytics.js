/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for the Assay admin template
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//vector map
	jQuery('#vmap').vectorMap(
	{ 
		map: 'world_en',
		backgroundColor : 'rgba(29, 197, 233, 0.7)',
		color: '#ffffff',
		borderColor: '#ffffff',
		selectedRegions: ['IN', 'US', 'RU', 'CN', 'DE', 'FR', 'GB'],
		selectedColor : 'rgba(245, 194, 43, 1)',
		onLabelShow: function(event, label, code)
		{
			if (code == 'us')
			{
				label.text('Users: 100');
			}
			else if(code == 'in')
			{
				label.text('Users: 234');
			}
			else if(code == 'gb')
			{
				label.text('Users: 34');
			}
			else if(code == 'ru')
			{
				label.text('Users: 90');
			}
			else if(code == 'cn')
			{
				label.text('Users: 76');
			}
			else if(code == 'fr')
			{
				label.text('Users: 64');
			}
			else if(code == 'de')
			{
				label.text('Users: 45');
			}
			else
			{
				label.text('Users: 78');
			}
		}
	});
	
	//social media Bar stats
	var socialMediaBarStats = new Chart( jQuery('#socialMediaBarStats'), {
		type: 'bar',
		data: {
			labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			datasets: [
				{
					data: [300, 320, 350, 340, 300, 360, 350],
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.8)'
				},
				{
					data: [350, 370, 400, 400, 350, 380, 400],
					backgroundColor: 'rgba(163, 137, 212, 0.8)',
					fill: 'zero',
				},
				{
					data: [400, 440, 450, 500, 400, 440, 460],
					fill: 'zero',
					backgroundColor: 'rgba(245, 194, 43, 0.8)'
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					},
					barPercentage: 0.7,
					categoryPercentage: 0.5,
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//traffic donut graph
	var resourceUsageDonut = new Chart( jQuery('#resourceUsageDonut'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo", "Others"],
			datasets: [
				{
					data: [
						40,
						20,
						30,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 70
		}
	});
	
	//live stats
	var count = 7;
	var data = {
		labels : ["1","2","3","4","5", "6", "7"],
		datasets: [
				{
					data: [25, 33, 40, 30, 20, 28, 33],
					borderColor: "#1dc5e9",
					lineTension: 0.4,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				},
				{
					data: [40, 45, 60, 50, 30, 45, 50],
					borderColor: "#1de986",
					lineTension: 0.4,
					backgroundColor: 'rgba(29, 233, 134, 0.1)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2
				}
			]
	  }
	var updateData = function(oldData){
		var labels = oldData["labels"];
		var pageViews = oldData["datasets"][0]["data"];
		var sessions = oldData["datasets"][1]["data"];
		labels.shift();
		count++;
		labels.push(count.toString());
		var newPageViews = Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 );
		var newSession = Math.floor(Math.random() * ( 30 - 15 + 1 ) + 15 );
		pageViews.push(newPageViews);
		sessions.push(newSession);
		pageViews.shift();
		sessions.shift();   
	};
	var ctx = document.getElementById("todayLiveStats").getContext("2d");
	var todayLiveStats = new Chart( jQuery('#todayLiveStats'), {
		type: 'line',
		data: data,
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});

	setInterval(function(){
		updateData(data);
		todayLiveStats.update()}, 2000);
		
		
		//country wise search engine
	var countrySearchEngineIn = new Chart( jQuery('.countrySearchEngineIn'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo", "Others"],
			datasets: [
				{
					data: [
						40,
						20,
						30,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise OS
	var countrySearchEngineIn = new Chart( jQuery('.countryOSIn'), {
		type: 'doughnut',
		data: {
			labels: ["Windows", "Mac", "Linux", "Others"],
			datasets: [
				{
					data: [
						50,
						20,
						20,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise search engine
	var countrySearchEngineGb = new Chart( jQuery('.countrySearchEngineGb'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo", "Others"],
			datasets: [
				{
					data: [
						40,
						20,
						30,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise OS
	var countrySearchEngineGb = new Chart( jQuery('.countryOSGb'), {
		type: 'doughnut',
		data: {
			labels: ["Windows", "Mac", "Linux", "Others"],
			datasets: [
				{
					data: [
						50,
						20,
						20,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise search engine
	var countrySearchEngineUs = new Chart( jQuery('.countrySearchEngineUs'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo", "Others"],
			datasets: [
				{
					data: [
						20,
						40,
						30,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise OS
	var countrySearchEngineUs = new Chart( jQuery('.countryOSUs'), {
		type: 'doughnut',
		data: {
			labels: ["Windows", "Mac", "Linux", "Others"],
			datasets: [
				{
					data: [
						20,
						50,
						20,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise search engine
	var countrySearchEngineCa = new Chart( jQuery('.countrySearchEngineCa'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo", "Others"],
			datasets: [
				{
					data: [
						35,
						20,
						35,
						10
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
	
	//country wise OS
	var countrySearchEngineCa = new Chart( jQuery('.countryOSCa'), {
		type: 'doughnut',
		data: {
			labels: ["Windows", "Mac", "Linux", "Others"],
			datasets: [
				{
					data: [
						30,
						40,
						15,
						15
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4',
						'#1de986'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			},
			cutoutPercentage: 0
		}
	});
});
