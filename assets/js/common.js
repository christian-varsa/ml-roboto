/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for the Assay admin template
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	
	//get the viewport details of Window
	var windowHeight = jQuery(window).height();
	var windowWidth = jQuery(window).width();
	
	//get the height of document
	var docheight = jQuery(document).height();
	//jQuery("#sideMenu").css("height", docheight + 'px' );
	jQuery("#sideMenu").css("height", (windowHeight - 53) + 'px' );
	
	//hide the vertical side menu if Width is less than 768px
	if(windowWidth < 768){
		jQuery('#sideMenu').css("position", "absolute");
		jQuery('#sideMenu').hide();
		jQuery('.content').css('margin-left', '0');
		jQuery('.top-nav').css('margin-left', '50px');
	}
	
	if(windowWidth > 768 && windowWidth < 992){
		jQuery('#sideMenu').addClass("sm-side-menu");
		jQuery('.content').addClass("sm-content");
		jQuery('#brand').addClass("sm-brand");
	}
	
	//check the window resize
	jQuery(window).resize(function() {
		windowWidth = jQuery(window).width();
		//hide the vertical side menu if Width is less than 768px
		if(windowWidth < 768){
			jQuery('#sideMenu').css("position", "absolute");
			jQuery('#sideMenu').hide();
			jQuery('.content').css('margin-left', '0');
			jQuery('.top-nav').css('margin-left', '50px');
		}
		else if(windowWidth > 768 && windowWidth < 992){
			jQuery('#sideMenu').css("position", "fixed");
			jQuery('#brand').addClass("sm-brand");
			jQuery('#sideMenu').addClass("sm-side-menu");
			jQuery('.content').addClass("sm-content");
			jQuery('.content').css('margin-left', '50');
		}
		else{
			jQuery('#brand').removeClass("sm-brand");
			jQuery('#sideMenu').removeClass("sm-side-menu");
			jQuery('.content').removeClass("sm-content");
			jQuery('#brand').addClass("brand");
			jQuery('#sideMenu').addClass("side-menu");
			jQuery('.content').addClass("content");
			jQuery('.content').css('margin-left', '240px');
		}
	});
	
	//toggle side menu
	jQuery('#toggleSideMenu').click(function(){
		if(windowWidth < 768){
			jQuery('#sideMenu').toggle("fast");
		}
		else{
			jQuery('#brand').toggleClass("sm-brand");
			jQuery('#sideMenu').toggleClass("sm-side-menu");
			jQuery('.content').toggleClass("sm-content");
		}
	});
	
	//toggle right side option panel
	jQuery('#rightOptionPanel').hide();
	jQuery('#toggleRightSideOption').click(function(){
		jQuery('#rightOptionPanel').toggle("fast");
	});
	
	//datepicker
	jQuery("#fromDate").datepicker();
	jQuery("#toDate").datepicker();
	
	//scroll
	jQuery('.assay-perfect-scroll').perfectScrollbar();
	jQuery('.assay-perfect-scroll').perfectScrollbar().mouseenter(function(){$(this).perfectScrollbar('update');})
	
	jQuery('#ordersTable_filter').find('input').attr("placeholder", "Search Orders")
	jQuery('#invoiceTable_filter').find('input').attr("placeholder", "Search Invoices")
	
	//set the right side option panel height
	jQuery('.side-option-wrapper').css("height", windowHeight + 'px' );
    jQuery(".green-dial").knob({
		"width" : 40,
		"height" : 40,
		"fgColor":"#1de986",
		"lineCap" : "round",
		"displayInput" : false,
		"readOnly" : true,
		"thickness" : 0.2
	});	
	
	jQuery(".yellow-dial").knob({
		"width" : 40,
		"height" : 40,
		"fgColor":"#f5c22b",
		"lineCap" : "round",
		"displayInput" : false,
		"readOnly" : true,
		"thickness" : 0.2
	});	
	
	//footer
	var dashContentHeight = jQuery(".dashboard-content").height();
	if(windowHeight > dashContentHeight){
		$(".footer-wrapper").css("margin-top", (windowHeight - dashContentHeight)+"px");
	}
	var colors = ["main", "blue", "purple", "orange", "light-green", "black", "green", "yellow"];
	var colorSwitcher = "<div class='color-switcher'><div class='cogs'><i class='fa fa-cogs' id='toggleSwitcher'></i></div><div class='colors hide-colors'>";
	var i = 0;
	for(i=0; i < colors.length; i ++) {
		colorSwitcher = colorSwitcher + "<div id='"+colors[i]+"' class='"+colors[i]+"'></div>";
	}
	colorSwitcher = colorSwitcher + "</div><div class='clearfix'></div></div>";
	//color switcher
	$(".dashboard-content").append(colorSwitcher);
	$("#toggleSwitcher").click(function(){
		$(".color-switcher").toggleClass("expand-swithcher");
		$(".colors").toggleClass("hide-colors");
	})
	
	$(".colors>div").click(function(){
		var path = "assets/css/color/"+$(this).attr('id')+".css";
		$("#selectedColor").attr('href', path);
	})
	
	//hide loading animation
	$(".page-loader-animation").hide();
});
