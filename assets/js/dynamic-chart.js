/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for line charts
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//label
	var label = ["Facebook", "Google", "Yahoo", "Bing", "Twitter"];
	
	var count = 5;
	var data = {
		labels : label,
		datasets: [
				{
					data: [25, 33, 40, 30, 20],
					borderColor: "#1dc5e9",
					lineTension: 0.4,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				},
				{
					data: [40, 45, 60, 50, 30],
					borderColor: "#1de986",
					lineTension: 0.4,
					backgroundColor: 'rgba(29, 233, 134, 0.1)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2
				}
			]
	  }
	var option = {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	var updateData = function(){
		data["datasets"][0]["data"] = [
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 )
									  ];
		data["datasets"][1]["data"] = [
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 ),
										Math.floor(Math.random() * ( 50 - 30 + 1 )+ 30 )
									];
	};
	//var ctx = document.getElementById("dynamicLine").getContext("2d");
	var dynamicLine = new Chart( jQuery('#dynamicLine'), {
		type: 'line',
		data: data,
		options: option
	});

	setInterval(function(){
			updateData(data);
			dynamicLine.update();
			dynamicBar.update();
		}, 2000);
	
	var dynamicBar = new Chart( jQuery('#dynamicBar'), {
		type: 'bar',
		data: data,
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					barPercentage: 1,
					categoryPercentage: 0.4,
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	//END
});
