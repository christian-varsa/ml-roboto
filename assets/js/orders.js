/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for the Assay admin template
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	
	//datatables
	jQuery('#ordersTable').DataTable(
	{
		"dom": "<'row'<'col clear-padding'f>>" + "t" + "<'row table-footer-info'<'col clear-padding'l><'col'i><'col'p>>",
		"language": {
						"search" : "",
						"paginate": {
							"first": "<<",
							"last":       ">>",
							"next":       ">",
							"previous":   "<"
						},
		},
		"lengthMenu": [5, 10, 25, 50],
	});
	jQuery('#invoiceTable').DataTable(
	{
		"dom": "<'row'<'col clear-padding'f>>" + "t" + "<'row table-footer-info'<'col clear-padding'l><'col'i><'col'p>>",
		"language": {
						"search" : "",
						"paginate": {
							"first": "<<",
							"last":       ">>",
							"next":       ">",
							"previous":   "<"
						},
		},
		"lengthMenu": [5, 10, 25, 50],
	});
	jQuery('#ordersTable_filter').find('input').attr("placeholder", "Search Orders")
	jQuery('#invoiceTable_filter').find('input').attr("placeholder", "Search Invoices")
});
