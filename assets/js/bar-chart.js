/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for line charts
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//label
	var label = ["Mon", "Tue", "Wed", "Thu", "Fri"];
	
	//dataset
	var dataset = [
					{
						data: [5, 10, 15, 10, 13],
						backgroundColor: "#1dc5e9"
					},
					{
						data: [3, 13, 10, 15, 8],
						backgroundColor: "#f5c22b",
					},
					{
						data: [8, 5, 7, 10, 4],
						backgroundColor: "#a389D4",
					},
				];
	var datasetOne = [
						{
							data: [8, 10, 15, 12, 13],
							backgroundColor: "#1dc5e9",
						},
						{
							data: [3, 13, 10, 15, 8],
							backgroundColor: "#f5c22b",
						},
						{
							data: [3, 5, 7, 10, 4],
							backgroundColor: "#a389D4",
						}
					]
	var datasetThree = [
						{
							data: [8, 10, 15, 12, 13],
							borderColor: "transparent",
							pointBackgroundColor: '#1dc5e9',
							pointBorderColor: '#1dc5e9',
							radius: 8,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						},
						{
							data: [3, 5, 7, 10, 4],
							borderColor: "transparent",
							pointBackgroundColor: '#f5c22b',
							pointBorderColor: '#f5c22b',
							radius: 8,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						}
					]
	//scales
	var xAxesScale = [{
						ticks: {
							display: true,
							beginAtZero:true,
						},
						gridLines: {
							display:false,
							drawBorder: false,
							color: '#eeeeee',
							zeroLineColor: '#eeeeee'
						},
						barPercentage: 0.7,
						categoryPercentage: 0.5,
					}];
	var yAxesScale = [{
						gridLines: {
							drawBorder: false,
							display:true
						},
						ticks: {
							display: true,
							beginAtZero:true,
						}
					}];
	var xAxesScaleOne = [{
						ticks: {
							display: true,
							beginAtZero:true,
						},
						stacked: true,
						gridLines: {
							display:false,
							drawBorder: false,
							color: '#eeeeee',
							zeroLineColor: '#eeeeee'
						},
						barPercentage: 0.7,
						categoryPercentage: 0.5,
					}];
	var yAxesScaleOne = [{
						gridLines: {
							drawBorder: false,
							display:true
						},
						stacked: true,
						ticks: {
							display: true,
							beginAtZero:true,
						}
					}];
	
	var xAxesScaleTwo = [{
						ticks: {
							display: true,
							beginAtZero:true,
						},
						stacked: true,
						gridLines: {
							display:true,
							drawBorder: false,
							color: '#eeeeee',
							zeroLineColor: '#eeeeee'
						},
					}];
	var yAxesScaleTwo = [{
						gridLines: {
							drawBorder: false,
							display:false
						},
						stacked: true,
						ticks: {
							display: true,
							beginAtZero:true,
						},
						barPercentage: 0.7,
						categoryPercentage: 0.5,
					}];
	
	var lineChartOne = new Chart( jQuery('#lineChartOne'), {
		type: 'bar',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0
				}
			}
		}
	});
	
	var lineChartTwo = new Chart( jQuery('#lineChartTwo'), {
		type: 'bar',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			scales: {
				xAxes: xAxesScaleOne,
				yAxes: yAxesScaleOne
			},
			legend: {
				display: false
			}
		}
	});
	
	var lineChartThree = new Chart( jQuery('#lineChartThree'), {
		type: 'horizontalBar',
		data: {
			labels: label,
			datasets: datasetOne
		},
		options: {
			scales: {
				xAxes: xAxesScaleTwo,
				yAxes: yAxesScaleTwo
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0.5
				}
			}
		}
	});
	
	var lineChartFour = new Chart( jQuery('#lineChartFour'), {
		type: 'horizontalBar',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0.5
				}
			}
		}
	});
	
	//END
});
