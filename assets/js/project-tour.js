/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file Tour
* Date: 11-11-2017
**/
jQuery(document).ready(function($){
	
	"use strict";
	// Instance the tour
	var tour = new Tour({
		name: "assay-tour",
		steps: [
			{
				element: "#projectTopLeadsStats",
				title: "Total Leads",
				content: "Shows the total leads for the current week."
			},
			{
				element: "#toggleRightSideOption",
				title: "Right Sidebar Toggle",
				content: "Toggle right sidebar option panel."
			},
			{
				element: "#tourProjectProgress",
				title: "Status of the projects.",
				content: "Horizontal stacked graph shows the progress of current projects."
			},
			{
				element: "#tourExpenses",
				title: "Expenses.",
				content: "Area graph shows the expenses & earnings."
			}
		],
		backdrop: true,
	});

	// Initialize the tour
	tour.init();

	// Start the tour
	tour.start();
	//END
});
