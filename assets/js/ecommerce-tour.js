/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file Tour
* Date: 11-11-2017
**/
jQuery(document).ready(function($){
	
	"use strict";
	// Instance the tour
	var tour = new Tour({
		name: "eCom-tour",
		steps: [
			{
				element: "#salesStatisticsWarp",
				title: "Sales Stats",
				content: "Shows the total sales & Orders"
			},
			{
				element: "#taskProgressWrap",
				title: "Task Progress",
				content: "Progress of tasks."
			},
			{
				element: "#saleStatsWrap",
				title: "Current day Sales Stats",
				content: "Sales stats for current calendar day."
			}
		],
		backdrop: true,
	});

	// Initialize the tour
	tour.init();

	// Start the tour
	tour.start();
	//END
});
