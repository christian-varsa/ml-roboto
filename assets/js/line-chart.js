/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for line charts
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//label
	var label = ["Mon", "Tue", "Wed", "Thu", "Fri"];
	
	//dataset
	var dataset = [
					{
						data: [5, 10, 15, 10, 13],
						borderColor: "#1dc5e9",
						//lineTension: 0,
						pointBackgroundColor: '#ffffff',
						pointBorderColor: '#1dc5e9',
						radius: 5,
						borderWidth: 2,
						fill: false
					},
					{
						data: [3, 13, 10, 15, 8],
						borderColor: "#f5c22b",
						//lineTension: 0,
						pointBackgroundColor: '#ffffff',
						pointBorderColor: '#f5c22b',
						radius: 5,
						borderWidth: 2,
						fill: false
					},
					{
						data: [8, 5, 7, 10, 4],
						borderColor: "#a389D4",
						//lineTension: 0,
						pointBackgroundColor: '#ffffff',
						pointBorderColor: '#a389D4',
						radius: 5,
						borderWidth: 2,
						fill: false
					},
				];
	var datasetOne = [
						{
							data: [8, 10, 15, 12, 13],
							borderColor: "#1dc5e9",
							pointBackgroundColor: '#ffffff',
							pointBorderColor: '#1dc5e9',
							radius: 5,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						},
						{
							data: [3, 5, 7, 10, 4],
							borderColor: "#a389D4",
							pointBackgroundColor: '#ffffff',
							pointBorderColor: '#a389D4',
							radius: 5,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						}
					]
	var datasetThree = [
						{
							data: [8, 10, 15, 12, 13],
							borderColor: "transparent",
							pointBackgroundColor: '#1dc5e9',
							pointBorderColor: '#1dc5e9',
							radius: 8,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						},
						{
							data: [3, 5, 7, 10, 4],
							borderColor: "transparent",
							pointBackgroundColor: '#f5c22b',
							pointBorderColor: '#f5c22b',
							radius: 8,
							borderWidth: 2,
							fill: false,
							steppedLine: true
						}
					]
	//scales
	var xAxesScale = [{
						ticks: {
							display: true,
							beginAtZero:true,
						},
						gridLines: {
							display:false,
							drawBorder: false,
							color: '#eeeeee',
							zeroLineColor: '#eeeeee'
						}
					}];
	var yAxesScale = [{
						gridLines: {
							drawBorder: false,
							display:true
						},
						//barPercentage: 0.2,
						categoryPercentage: 0.5,
						ticks: {
							display: true,
							beginAtZero:true,
						}
					}]
	
	
	var lineChartOne = new Chart( jQuery('#lineChartOne'), {
		type: 'line',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0
				}
			}
		}
	});
	
	var lineChartTwo = new Chart( jQuery('#lineChartTwo'), {
		type: 'line',
		data: {
			labels: label,
			datasets: dataset
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0.5
				}
			}
		}
	});
	
	var lineChartThree = new Chart( jQuery('#lineChartThree'), {
		type: 'line',
		data: {
			labels: label,
			datasets: datasetOne
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0.5
				}
			}
		}
	});
	
	var lineChartFour = new Chart( jQuery('#lineChartFour'), {
		type: 'line',
		data: {
			labels: label,
			datasets: datasetThree
		},
		options: {
			scales: {
				xAxes: xAxesScale,
				yAxes: yAxesScale
			},
			legend: {
				display: false
			},
			elements: {
				line: {
					tension: 0.5
				}
			}
		}
	});
	
	//END
});
