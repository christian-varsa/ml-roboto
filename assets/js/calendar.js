/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for full calendar
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function(){
	
	"use strict";
/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2017-10-10',
			editable: true,
			droppable: true,
			eventLimit: true,
			events: [
				{
					title: 'All Day Event',
					start: '2017-10-01',
					backgroundColor: '#f5c22b',
				},
				{
					title: 'Long Event',
					start: '2017-10-07',
					end: '2017-10-10',
					backgroundColor: '#a389D4'
				},
				{
					title: 'Conference',
					start: '2017-10-11',
					end: '2017-10-13',
					backgroundColor: '#f5c22b'
				},
				{
					title: 'Meeting',
					start: '2017-10-12T10:30:00',
					end: '2017-10-12T12:30:00',
					backgroundColor: '#a389D4'
				},
				{
					title: 'Lunch',
					start: '2017-10-12T12:00:00',
					backgroundColor: '#1de986'
				},
				{
					title: 'Meeting',
					start: '2017-10-12T14:30:00',
					backgroundColor: '#a389D4'
				},
				{
					title: 'Happy Hour',
					start: '2017-10-12T17:30:00',
					backgroundColor: '#1de986'
				},
				{
					title: 'Dinner',
					start: '2017-10-12T20:00:00',
					backgroundColor: '#1de986'
				},
				{
					title: 'Birthday Party',
					start: '2017-10-13T07:00:00',
					backgroundColor: '#1de986'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2017-10-28',
					backgroundColor: '#f5c22b',
				}
			],
			eventBorderColor: 'transparent',
			eventTextColor: '#fff', 
			drop: function() {
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			}
		});
});