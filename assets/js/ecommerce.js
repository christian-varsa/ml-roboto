/**
* Author: LimpidThemes
* Version: 1.0
* Description: Javascript file for the Assay admin template
* Date: 03-09-2017
**/

/**********************************************************
		BEGIN: MENU
**********************************************************/
jQuery(document).ready(function($){
	
	"use strict";
	//vector map
	jQuery('#vmap').vectorMap(
	{ 
		map: 'world_en',
		backgroundColor : 'rgba(29, 197, 233, 0.7)',
		color: '#ffffff',
		borderColor: '#ffffff',
		selectedRegions: ['IN', 'US', 'RU', 'CN', 'DE', 'FR', 'GB'],
		selectedColor : 'rgba(245, 194, 43, 1)',
		onLabelShow: function(event, label, code)
		{
			if (code == 'us')
			{
				label.text('Users: 100');
			}
			else if(code == 'in')
			{
				label.text('Users: 234');
			}
			else if(code == 'gb')
			{
				label.text('Users: 34');
			}
			else if(code == 'ru')
			{
				label.text('Users: 90');
			}
			else if(code == 'cn')
			{
				label.text('Users: 76');
			}
			else if(code == 'fr')
			{
				label.text('Users: 64');
			}
			else if(code == 'de')
			{
				label.text('Users: 45');
			}
			else
			{
				label.text('Users: 78');
			}
		}
	});
	
	//page views line & bar chart
	var chartData = {
            labels: ["Jan", "Feb", "Mar", "Apr"],
            datasets: [{
                type: 'line',
				label: 'Session',
                data: [10, 15, 10, 15],
				borderColor: "#1dc5e9",
				lineTension: 0,
				pointBackgroundColor: '#ffffff',
				pointBorderColor: '#1dc5e9',
				radius: 5,
				borderWidth: 2,
				fill: 'false'
            }, {
                type: 'bar',
                label: 'Views',
                backgroundColor: '#f5c22b',
                data: [9, 12, 9, 12],
                borderColor: 'white',
                borderWidth: 2
			}]
    };
	var ctx = document.getElementById("barLineStats").getContext("2d");
    var barLineStats = new Chart(ctx, {
                type: 'bar',
                data: chartData,
                options: {
                    responsive: true,
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    },
					scales: {
						xAxes: [{
						ticks: {
							display: true,
							beginAtZero:true,
						},
						gridLines: {
							display:false,
							drawBorder: false,
							color: '#eeeeee',
							zeroLineColor: '#eeeeee'
						}
						}],
						yAxes: [{
							gridLines: {
								drawBorder: false,
								display:true
							},
							//barPercentage: 0.2,
							categoryPercentage: 0.5,
							ticks: {
								display: true,
								beginAtZero:true,
							}
						}]
					},
					legend: {
							display: false
					}
				}
    });
	//Today Stats
	var todayStatsTab = new Chart( jQuery('#todayStatsTab'), {
		type: 'line',
		data: {
			labels: ["10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM"],
			datasets: [
				{
					data: [2, 11, 9, 12, 8, 15, 12],
					borderColor: "#1dc5e9",
					lineTension: 0,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				},
				{
					data: [6, 15, 13, 17, 13, 19, 20],
					borderColor: "#1de986",
					lineTension: 0,
					backgroundColor: 'rgba(29, 233, 134, 0.1)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//Weekly Stats
	var weeklyStatsTab = new Chart( jQuery('#weeklyStatsTab'), {
		type: 'line',
		data: {
			labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			datasets: [
				{
					data: [25, 33, 40, 30, 20, 28, 33],
					borderColor: "#1dc5e9",
					lineTension: 0,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				},
				{
					data: [40, 45, 60, 50, 30, 45, 50],
					borderColor: "#1de986",
					lineTension: 0,
					backgroundColor: 'rgba(29, 233, 134, 0.1)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	//yearly Stats
	var yearlyStatsTab = new Chart( jQuery('#yearlyStatsTab'), {
		type: 'line',
		data: {
			labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			datasets: [
				{
					data: [400, 440, 420, 500, 470, 380, 460],
					borderColor: "#1dc5e9",
					lineTension: 0,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
					fill: 'zero',
					backgroundColor: 'rgba(29, 197, 233, 0.1)'
				},
				{
					data: [300, 320, 350, 340, 300, 360, 350],
					borderColor: "#1de986",
					lineTension: 0,
					backgroundColor: 'rgba(29, 233, 134, 0.1)',
					fill: 'zero',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: true,
						beginAtZero:true,
					},
					gridLines: {
						display:false,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:true
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: true,
						beginAtZero:true,
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	//weekly page view stats
	var weeklyCommentsStats = new Chart( jQuery('#weeklyCommentsStats'), {
		type: 'bar',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [90, 120, 100, 80, 90, 70, 130],
					borderColor: "#f5c22b",
					fill: 'start',
					backgroundColor: '#f5c22b',
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.5,
					gridLines: {
						display:false,
						drawBorder: false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//top page views stats graphs
	var statPageViewGraph = new Chart( jQuery('#statPageViewGraph'), {
		type: 'line',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [0, 11, 9, 12, 8, 15, 0],
					borderColor: "#1de986",
					fill: 'start',
					lineTension: 0,
					backgroundColor: 'rgba(29, 233, 134, 0.3)',
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1de986',
					radius: 5,
					borderWidth: 2,
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false
					},
					gridLines: {
						display:false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	/*
	//traffic donut graph
	var resourceUsageDonut = new Chart( jQuery('#resourceUsageDonut'), {
		type: 'doughnut',
		data: {
			labels: ["Google", "Bing", "Yahoo"],
			datasets: [
				{
					data: [
						45,
						25,
						30
					],
					backgroundColor: [
						'#f5c22b',
						'#1dc5e9',
						'#a389D4'
					],
				}
			]
		},
		options: {
			legend: {
				display: false
			}
		}
	});*/
	
	//facebook like stats graphs
	var facebookLikeBar = new Chart( jQuery('#facebookLikeBar'), {
		type: 'bar',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [100, 150, 90, 120, 80, 150, 130],
					borderColor: "#1dc5e9",
					fill: 'start',
					backgroundColor: '#1dc5e9',
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false,
					},
					barPercentage: 0.5,
					gridLines: {
						display:false,
						drawBorder: false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//facebook like stats graphs
	var twitterLikeBar = new Chart( jQuery('#twitterLikeBar'), {
		type: 'bar',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [120, 90, 100, 120, 140, 150, 130],
					borderColor: "#a389D4",
					fill: 'start',
					backgroundColor: '#a389D4',
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.5,
					gridLines: {
						display:false,
						drawBorder: false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//top weekly sales stats graphs
	var weeklySales = new Chart( jQuery('#weeklySales'), {
		type: 'line',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [8, 11, 9, 12, 8, 15, 12],
					borderColor: "#1dc5e9",
					fill: false,
					lineTension: 0,
					pointBackgroundColor: '#ffffff',
					pointBorderColor: '#1dc5e9',
					radius: 5,
					borderWidth: 2,
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						display: false
					},
					gridLines: {
						display:true,
						drawBorder: false,
						color: '#eeeeee',
						zeroLineColor: '#eeeeee'
					}
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false,
						display:false,
					},
					//barPercentage: 0.2,
					categoryPercentage: 0.5,
					ticks: {
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//weekly page view stats
	var weeklyPageViewStats = new Chart( jQuery('#weeklyPageViewStats'), {
		type: 'bar',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [120, 90, 100, 120, 140, 150, 130],
					borderColor: "#a389D4",
					fill: 'start',
					backgroundColor: '#a389D4',
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.5,
					gridLines: {
						display:false,
						drawBorder: false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	
	//weekly page view stats
	var weeklyPagelikeStats = new Chart( jQuery('#weeklyPagelikeStats'), {
		type: 'bar',
		data: {
			labels: ["21 Jul", "22 Jul", "23 Jul", "24 Jul", "25 Jul", "26 Jul", "27 Jul"],
			datasets: [
				{
					data: [90, 120, 100, 80, 90, 70, 130],
					borderColor: "#1dc5e9",
					fill: 'start',
					backgroundColor: '#1dc5e9',
				}
			]
		},
		options: {
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.5,
					gridLines: {
						display:false,
						drawBorder: false
					}
				}],
				yAxes: [{
					gridLines: {
						display:false,
						drawBorder: false,
					},
					ticks: {
						beginAtZero:true,
						display: false
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
	//datatables
	jQuery('#ordersTable').DataTable(
	{
		"dom": "<'row'<'col clear-padding'f>>" + "t" + "<'row table-footer-info'<'col clear-padding'l><'col'i><'col'p>>",
		"language": {
						"search" : "",
						"paginate": {
							"first": "<<",
							"last":       ">>",
							"next":       ">",
							"previous":   "<"
						},
		},
		"lengthMenu": [5, 10, 25, 50],
	});
	jQuery('#invoiceTable').DataTable(
	{
		"dom": "<'row'<'col clear-padding'f>>" + "t" + "<'row table-footer-info'<'col clear-padding'l><'col'i><'col'p>>",
		"language": {
						"search" : "",
						"paginate": {
							"first": "<<",
							"last":       ">>",
							"next":       ">",
							"previous":   "<"
						},
		},
		"lengthMenu": [5, 10, 25, 50],
	});
	jQuery('#ordersTable_filter').find('input').attr("placeholder", "Search Orders")
	jQuery('#invoiceTable_filter').find('input').attr("placeholder", "Search Invoices")
});
