<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <title>Assay - Responsive Admin Template</title>
    <base href="<?php echo base_url() ?>">

    <!-- Styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/style.css" rel="stylesheet" media="screen">
    <link id="selectedColor" href="assets/css/color/main.css" rel="stylesheet" media="screen">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">

</head>
<body>

<div class="page-loader-animation">
    <div class="wrapper">
        <div class="first"></div>
        <div class="second"></div>
        <div class="third"></div>
    </div>
</div>


<!-- Top Header Section -->
<div class="row">
    <div id="brand" class="brand" >
        <span class="brand-logo"><i class="fa fa-cogs"></i></span>

        <a class="brand-text" href="<?php echo base_url().'HomeController/index' ?>" style="
    color: white;">MROBOT</a>

    </div>
    <div class="container-fluid content top-nav">
        <ul class="float-right nav-right-link">
            <li>
                <div class="item user">
                    <img src="assets/img/ml.png" alt="user">
                </div>
            </li>
            <li>
                <div class="item setting">
                    <ul>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </a>
                            <div class="dropdown-menu setting-dropdown">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fa fa-user"></i> Perfil</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-comment-o"></i> Mensajes automáticos</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-tasks"></i> Mi cuenta</a>
                                    </li>
                                    <li class="last-setting">
                                        <a href="<?php echo base_url().'HomeController/logout' ?>"><i class="fa fa-sign-out"></i> Salir</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- End Top Header Section -->

<!-- Content Section -->
<div class="row main-content-wrapper">
    <div id="sideMenu" class="vertical-side-menu side-menu assay-perfect-scroll">
        <ul>


            <li class="side-menu-header"><span>DASHBOARD</span></li>


            <li><a  href="<?php echo base_url().'HomeController/index' ?>" class="side-menu-link"><i
                            class="fa fa-bar-chart"></i><span>Home<span
                                class="pull-right nav-link-info error-info">New</span></span></a></li>

            <li><a  href="<?php echo base_url().'CustomMessagesController/index' ?>" class="side-menu-link"><i class="fa fa-comments-o"></i><span>Mensajes automaticos</span></a></li>
        </ul>
    </div>
    <div class="container-fluid content dashboard-content">
        <div class="row">
            <div class="col-12">
                <div class="dashboard-widget-item page-title">
                    <div class="row">
                        <div class="col-6">
                            <h4>Mensajes automáticos</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"></div>
        <div class="row"></div>

        <div class="row">
            <div class="col-12">
                <div class="widget-item dashboard-widget-item chart-widget">
                    <p class="widget-item-title">Puedes escribir @COMPRADOR en el mensaje para que se complete
                        automáticamente con el apodo de quien realizó la compra.</p>
                    <div class="form-with-input-icon">

                        <?php echo form_open(base_url() . 'CustomMessagesController/save_messages') ?>

                        <div class="row">
                            <div class="col-12">
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input" name="first_message_checked">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Enviar automáticamente un mensaje al comprador apenas haya realizado su compra (ya su pago estará acreditado)</span>
                                </label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div class="input-group-addon"></div>
                                    <textarea rows="7" maxlength="3400" type="mensaje1" required class="form-control"
                                              name="first_message"><?php echo $first_message?></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input" name="second_message_checked">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Enviar automáticamente un mensaje al comprador a la salida del pedido.</span>
                                </label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div class="input-group-addon"></div>
                                    <textarea rows="7" maxlength="3400" type="mensaje1" required class="form-control"
                                              name="second_message"><?php echo $second_message?></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input" name="third_message_checked">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Enviar automáticamente un mensaje al comprador a la llegada del pedido.</span>
                                </label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div class="input-group-addon"></div>
                                    <textarea rows="7" maxlength="3400" type="mensaje1" required class="form-control"
                                              name="third_message"><?php echo $third_message?></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="login-btn-wrapper text-center">
                            <input class="login-btn" type="submit" value="Guardar cambios">

                        </div>

                        <? echo form_close() ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- Start Footer -->
        <div class="footer-wrapper">
            &copy; Copyright Assay
        </div>
    </div>
</div>
<!-- End Content Section -->

<!-- Scripts -->
<script src="assets/js/jQuery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/plugins/jquery.knob.min.js"></script>
<script src="assets/js/common.js"></script>

</body>
</html>