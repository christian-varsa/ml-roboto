<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- SITE TITLE -->
    <title>Mercado Robot - Vende más en Mercado Libre</title>
    <meta name="description"
          content="Te ofrecemos herramientas de preguntas y gestión para que reacciones en tiempo real y optimices tu negocio.">
    <meta name="keywords"
          content="mercado libre, vender en mercado libre, preguntas mercado libre, mercado libre, aplicaciones mercado libre">
    <meta name="author" content="Mercado Robot">
    <base href="<?php echo base_url() ?>">
    <!-- PLUGINS CSS STYLE -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/slick/slick.css" rel="stylesheet" media="screen">
    <link href="assets/plugins/slick/slick-theme.css" rel="stylesheet" media="screen">
    <link href="assets/plugins/animate/animate.css" rel="stylesheet">
    <link href="assets/plugins/fancybox/jquery.fancybox.min.css" rel="stylesheet">
    <link href="assets/plugins/elegant_font/style.css" rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link href="assets/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/default.css" id="option_color">

    <!-- FAVICON -->
    <link href="assets/img/favicon.png" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="body" class="up-scroll">
<!-- ====================================
——— PRELOADER
===================================== -->
<div id="preloader" class="smooth-loader-wrapper">
    <div class="smooth-loader">
        <div class="spinner-eff spinner-eff-1">
            <div class="bar bar-top"></div>
            <div class="bar bar-right"></div>
            <div class="bar bar-bottom"></div>
            <div class="bar bar-left"></div>
        </div>
    </div>
</div>

<!-- ====================================
——— HEADER
===================================== -->
<header id="pageTop" class="header">
    <nav class="navbar navbar-expand-md navbar-scrollUp navbar-transparent navbar-sticky">
        <div class="container">
            <a class="navbar-brand" href="https://www.mercadorobot.com/">
                <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                     width="200.000000pt" height="100.000000pt" viewBox="0 0 474.000000 100.000000"
                     preserveAspectRatio="xMidYMid meet">
                    <metadata>
                    </metadata>
                    <g transform="translate(0.000000,100.000000) scale(0.100000,-0.100000)"
                       fill="#FFFFFF" stroke="none">
                        <path d="M1011 1461 c-6 -10 -23 -24 -38 -30 -16 -6 -50 -25 -78 -42 -27 -16
-63 -37 -80 -45 -16 -8 -39 -21 -50 -27 -11 -7 -63 -37 -115 -67 -52 -30 -103
-59 -112 -65 -9 -5 -36 -21 -60 -33 -45 -25 -88 -60 -88 -73 0 -12 63 -69 76
-69 7 0 30 -12 51 -26 21 -14 47 -30 58 -35 11 -5 34 -18 50 -28 17 -11 50
-31 75 -44 25 -14 61 -34 80 -45 19 -11 55 -32 80 -45 25 -14 61 -34 80 -45
19 -11 53 -30 75 -42 22 -12 53 -35 69 -51 63 -61 61 -69 58 348 -2 362 -3
379 -23 403 -11 14 -27 37 -37 52 -20 33 -56 37 -71 9z"/>
                        <path d="M1346 1332 c-4 -7 -9 -367 -6 -460 0 -9 16 -12 58 -10 l57 3 3 110
c1 60 6 113 11 118 5 5 20 -12 35 -37 24 -41 31 -46 62 -46 24 0 37 5 41 18
11 29 46 71 55 66 4 -3 8 -57 8 -120 l0 -115 58 3 57 3 0 235 0 235 -51 3
c-59 3 -64 -1 -120 -106 -23 -42 -45 -71 -53 -70 -7 2 -27 28 -43 58 -17 30
-36 62 -43 70 -6 8 -15 23 -18 33 -5 13 -18 17 -56 17 -27 0 -52 -4 -55 -8z"/>
                        <path d="M1916 1321 c-47 -31 -56 -68 -56 -221 0 -218 21 -244 195 -238 l100
3 0 45 0 45 -73 3 c-66 3 -74 5 -84 27 -8 19 -8 31 0 50 10 22 18 24 84 27
l73 3 0 45 0 45 -78 3 c-85 3 -99 13 -79 57 10 22 18 24 84 27 l73 3 0 45 0
45 -105 3 c-90 2 -109 -1 -134 -17z"/>
                        <path d="M2240 1320 c-7 -12 -9 -98 -8 -237 l3 -218 60 0 60 0 3 75 c1 41 5
81 7 88 9 26 40 -6 80 -85 l41 -78 58 -3 c33 -2 63 1 68 6 6 6 1 23 -11 43
-56 96 -75 148 -56 155 54 21 75 164 32 215 -41 49 -75 59 -206 59 -109 0
-121 -2 -131 -20z m218 -100 c12 -12 22 -27 22 -35 0 -24 -51 -56 -84 -53 -28
3 -31 7 -34 42 -2 21 -1 44 2 52 8 21 69 17 94 -6z"/>
                        <path d="M2760 1321 c-71 -38 -100 -102 -100 -221 0 -173 65 -244 218 -238
l77 3 3 54 3 54 -49 -7 c-90 -12 -122 23 -122 134 0 111 32 146 122 134 l49
-7 -3 54 -3 54 -80 2 c-62 2 -88 -2 -115 -16z"/>
                        <path d="M3107 1318 c-23 -22 -32 -50 -72 -238 -8 -36 -20 -85 -26 -110 -6
-25 -12 -58 -13 -75 -1 -29 0 -30 53 -33 62 -4 72 3 86 61 l11 42 54 0 54 0
11 -40 c5 -22 14 -46 18 -53 10 -14 113 -17 122 -3 3 6 -1 41 -10 78 -9 37
-22 97 -30 133 -55 257 -57 260 -165 260 -58 0 -73 -4 -93 -22z m117 -139 c3
-17 6 -49 6 -71 0 -38 -1 -39 -32 -36 -36 3 -36 2 -23 91 5 36 10 47 24 47 13
0 21 -10 25 -31z"/>
                        <path d="M3462 1328 c-17 -17 -17 -439 0 -456 8 -8 50 -12 128 -12 101 1 120
4 154 24 22 12 50 40 62 62 20 34 23 53 23 154 0 101 -3 120 -23 154 -12 22
-40 50 -62 62 -34 20 -53 23 -154 24 -78 0 -120 -4 -128 -12z m199 -98 c31
-17 49 -64 49 -130 0 -89 -32 -140 -89 -140 -32 0 -41 31 -41 140 0 56 5 110
10 121 12 21 41 25 71 9z"/>
                        <path d="M3968 1320 c-20 -11 -47 -36 -60 -57 -21 -35 -23 -49 -23 -163 0
-114 2 -128 24 -163 32 -53 85 -77 165 -77 92 0 138 25 171 95 21 45 25 68 25
145 0 118 -29 187 -91 219 -53 28 -158 28 -211 1z m142 -100 c24 -18 25 -24
25 -120 0 -96 -1 -102 -25 -120 -33 -26 -46 -25 -75 5 -23 22 -25 32 -25 115
0 83 2 93 25 115 29 30 42 31 75 5z"/>
                        <path d="M571 511 c-2 -309 -1 -320 20 -355 23 -40 48 -47 56 -16 3 10 14 22
25 26 11 3 52 25 91 48 40 23 79 46 87 50 8 4 24 12 35 19 11 7 45 26 75 44
67 38 104 76 120 124 11 30 10 40 -4 67 -19 37 -71 80 -141 118 -74 40 -97 53
-130 74 -16 11 -37 23 -45 27 -8 4 -48 26 -88 50 -41 24 -80 43 -87 43 -11 0
-13 -62 -14 -319z"/>
                        <path d="M3900 772 c-6 -2 -10 -12 -10 -23 0 -17 8 -19 74 -19 49 0 77 -4 85
-13 6 -9 12 -84 13 -203 3 -181 4 -189 23 -189 19 0 20 8 23 189 1 119 7 194
13 203 8 9 36 13 86 13 57 0 75 3 80 15 3 9 3 18 1 21 -6 5 -372 11 -388 6z"/>
                        <path d="M1352 548 l3 -223 25 0 c24 0 25 3 28 70 2 38 8 75 14 82 6 8 32 13
64 13 52 0 54 -1 94 -51 22 -28 48 -64 57 -80 18 -31 51 -46 78 -35 12 4 3 22
-42 82 l-57 76 41 38 c70 64 73 160 7 218 l-37 32 -139 0 -138 0 2 -222z m248
167 c17 -9 36 -31 44 -50 13 -30 13 -40 1 -68 -19 -46 -61 -67 -135 -67 -94 0
-100 6 -100 100 0 65 3 81 18 89 27 16 139 13 172 -4z"/>
                        <path d="M2042 740 c-23 -17 -55 -49 -72 -72 -27 -37 -30 -49 -30 -114 0 -110
36 -167 137 -218 44 -22 144 -21 186 2 109 60 131 95 132 212 0 79 -2 88 -30
124 -16 22 -48 52 -69 68 -36 26 -46 28 -126 28 -81 0 -90 -2 -128 -30z m191
-25 c19 -8 47 -28 63 -44 33 -35 59 -123 44 -151 -6 -10 -10 -27 -10 -38 0
-33 -75 -99 -120 -107 -124 -21 -212 52 -212 176 0 80 33 132 104 163 43 19
87 19 131 1z"/>
                        <path d="M2642 548 l3 -223 141 -3 142 -3 43 34 c41 30 44 37 47 84 3 45 0 54
-28 81 -36 36 -37 44 -9 82 31 41 22 113 -17 146 -27 22 -37 23 -177 24 l-147
0 2 -222z m262 171 c43 -20 49 -88 10 -124 -24 -22 -36 -25 -108 -25 -51 0
-87 5 -94 12 -16 16 -16 120 0 136 15 15 158 16 192 1z m29 -212 c20 -17 27
-32 27 -58 0 -57 -25 -72 -129 -77 -120 -6 -131 1 -131 82 0 34 3 66 7 69 3 4
50 7 103 7 86 0 100 -2 123 -23z"/>
                        <path d="M3353 751 c-50 -28 -62 -41 -94 -96 -65 -114 -17 -254 112 -319 56
-29 159 -19 225 22 85 53 125 166 93 263 -14 41 -75 119 -93 119 -8 0 -19 7
-26 15 -19 23 -174 20 -217 -4z m178 -35 c121 -51 146 -208 49 -303 -78 -76
-223 -44 -271 61 -67 148 76 304 222 242z"/>
                    </g>
                </svg>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent"
                    aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="icon_menu"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto site_nav scrolling">

                    <li class="nav-item">
                        <a class="nav-link " href="#about"> Quiénes somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#feature"> Herramientas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faqs">Preguntas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#price">Precios</a>
                    </li>
                    <a class="btn btn-outline-white" href="<?php echo AUTHORIZATION_CODE_URI ?>"> Probar Gratis</a>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- ====================================
——— MAIN WRAPPER
===================================== -->
<div class="main-wrapper home">


    <!-- ====================================
    ——— BANNER
    ===================================== -->
    <section class="bg-gradient banner-section">
        <div class="bubbles-section">
            <div class="bubbles-container">
                <div class="banner-bg-right">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="595.25" height="480.156" viewBox="0 0 595.25 480.156">
                        <path d="M193.731,445.760 L20.489,272.518 C-6.847,245.182 -6.847,200.860 20.489,173.523 L456.774,-262.759 C484.111,-290.096 528.433,-290.096 555.769,-262.759 L729.010,-89.518 C756.347,-62.182 756.347,262.138 729.010,289.474 L305.250,473.999 C265.528,489.874 221.067,473.095 193.731,445.760 Z"
                              class="right-bg-svg"/>
                    </svg>
                </div>
                <div id="circle-1" class="single-bubbles wow slideInLeft" data-wow-duration="1s" data-wow-delay="1s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="127" height="151" viewBox="0 0 127 151">
                        <circle class="circle" cx="45" cy="69" r="82"/>
                    </svg>
                </div>
                <div id="circle-2" class="single-bubbles wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.2s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="95" height="95" viewBox="0 0 95 95">
                        <circle class="circle" cx="47.5" cy="47.5" r="47.5"/>
                    </svg>
                </div>
                <div id="circle-3" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.5s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="61" height="61" viewBox="0 0 61 61">
                        <circle class="circle" cx="30.5" cy="30.5" r="30.5"/>
                    </svg>
                </div>

                <div id="circle-4" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".8s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="186" height="186" viewBox="0 0 186 186">
                        <circle class="circle" cx="93" cy="93" r="93"/>
                    </svg>
                </div>
                <div id="circle-5" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="95" height="95" viewBox="0 0 95 95">
                        <circle class="circle" cx="47.5" cy="47.5" r="47.5"/>
                    </svg>
                </div>
                <div id="circle-6" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.5s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="111" height="111" viewBox="0 0 111 111">
                        <circle class="circle" cx="55.5" cy="55.5" r="55.5"/>
                    </svg>
                </div>
                <div id="circle-7" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="95" height="95" viewBox="0 0 95 95">
                        <circle class="circle" cx="47.5" cy="47.5" r="47.5"/>
                    </svg>
                </div>
                <div id="circle-8" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.3s">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="95" height="95" viewBox="0 0 95 95">
                        <circle class="circle" cx="47.5" cy="47.5" r="47.5"/>
                    </svg>
                </div>
                <div id="circle-9" class="single-bubbles wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                    <svg id="circle-9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="164" height="164" viewBox="0 0 164 164">
                        <circle class="circle" cx="82" cy="82" r="82"/>
                    </svg>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row vh100 align-items-center justify-content-between">
                <div class="col-md-6 col-lg-7 wow fadeInLeft">
                    <div class="banner-text-content">
                        <h5>Aumenta tus ventas en</h5>
                        <h1>Mercado Libre</h1>
                        <p>Te ofrecemos herramientas de preguntas y gestión para que reacciones en tiempo real y
                            optimices tu negocio.</p>
                        <a href="#" class="btn-icon mr-2">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="31" height="38" viewBox="0 0 31 38">
                                    <path d="M25.892,20.190 C25.844,15.377 29.824,13.070 30.002,12.955 C27.766,9.689 24.282,9.241 23.041,9.189 C20.076,8.890 17.256,10.932 15.751,10.932 C14.251,10.932 11.929,9.234 9.470,9.278 C6.239,9.326 3.259,11.154 1.595,14.043 C-1.762,19.857 0.735,28.472 4.008,33.188 C5.607,35.496 7.513,38.089 10.017,37.996 C12.427,37.900 13.339,36.439 16.254,36.439 C19.168,36.439 19.987,37.996 22.538,37.949 C25.133,37.900 26.776,35.596 28.364,33.280 C30.200,30.603 30.956,28.009 31.001,27.877 C30.943,27.850 25.942,25.938 25.892,20.190 ZM21.099,6.067 C22.427,4.458 23.324,2.226 23.080,-0.000 C21.166,0.076 18.846,1.272 17.473,2.877 C16.240,4.301 15.162,6.574 15.451,8.756 C17.588,8.923 19.769,7.673 21.099,6.067 Z"
                                          class="brand-icon-svg"/>
                                </svg>
                            </div>
                            <div class="text">
                                <h6>Disponible en</h6>
                                <span>App Store</span>
                            </div>
                        </a>
                        <a href="#" class="btn-icon">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="33" height="37" viewBox="0 0 33 37">
                                    <path d="M30.975,14.967 L6.016,0.534 C5.409,0.184 4.719,-0.001 4.021,-0.001 C3.313,-0.001 2.615,0.189 2.003,0.548 C0.768,1.274 0.000,2.621 0.000,4.066 L0.000,32.932 C0.000,34.377 0.768,35.724 2.003,36.450 C2.615,36.809 3.313,36.999 4.021,36.999 C4.719,36.999 5.409,36.813 6.016,36.463 L30.974,22.029 C32.224,21.308 33.000,19.955 33.000,18.499 C33.000,17.043 32.224,15.690 30.975,14.967 ZM20.159,11.555 L16.732,16.384 L8.535,4.832 L20.159,11.555 ZM5.000,33.999 L16.732,20.613 L20.159,25.443 L5.000,33.999 ZM29.764,19.888 L22.287,24.212 L18.233,18.499 L22.287,12.785 L29.764,17.109 C30.256,17.394 30.562,17.926 30.562,18.499 C30.562,19.072 30.256,19.604 29.764,19.888 Z"
                                          class="brand-icon-svg"/>
                                </svg>
                            </div>
                            <div class="text">
                                <h6>Disponible en</h6>
                                <span>Google Play</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-5 col-lg-3 ">
                    <div class="d-none d-md-block banner-img wow fadeInRight">
                        <div class="col-md-5 col-lg-3 ">
                            <div class="d-none d-md-block banner-img wow fadeInRight"><img
                                        src="assets/img/banner/banner-img-01.png" alt="Banner Image"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Scroll btn -->
        <div class="scroll-btn-aria scrolling">
            <a href="#about" class="btn-scroll-down"><i class="arrow_down animated " aria-hidden="true"></i></a>
        </div>
    </section>


    <!-- ====================================
    ——— AMAZING FEATURES
    ===================================== -->
    <section class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-12 col-md-6">
                    <h3 class="section-title-center text-center">Amazing Features</h3>
                    <p class="text-center mt-2 mb-2">Sint nulla facilisi auctor diam ducimus, at irure! aliquip anim
                        tempus laudantium odit hymenaeos.</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="media media-style-01 flex-column wow tafFadeInUp">
                        <div class="icon-style-1 mb-4 m-auto">
                            <div class="icon">
                                <i class="icon_lightbulb_alt" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body text-center">
                            <h5 class="mt-4">Creative Desing</h5>
                            <p>Sed libero odio phasellus malesuada, mi a ante sed donec, lacinia facilisis vitae velit
                                suspendisse. Tellus elit lectus lobortis. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="media media-style-01 flex-column wow tafFadeInUp ">
                        <div class="icon-style-1 mb-4 m-auto">
                            <div class="icon">
                                <i class="icon_toolbox_alt" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body text-center">
                            <h5 class="mt-4">Unlimited Dashboards</h5>
                            <p>Purus magna turpis enim, vitae vitae in a, suspendisse sit vulputate massa, a non euismod
                                elit nunc. Rerum dui convallis sed cras</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="media media-style-01 flex-column wow tafFadeInUp">
                        <div class="icon-style-1 mb-4 m-auto">
                            <div class="icon">
                                <i class="icon_pencil-edit" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="media-body text-center">
                            <h5 class="mt-4">easy to used</h5>
                            <p>Aut augue imperdiet pharetra lectus egest as perferendis aliquid nunc.sed do eiusmod
                                tempor incididunt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— ABOUT OUR APP
    ===================================== -->
    <section class="section-padding bg-whiteSmoke" id="about">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6 col-12 wow tafFadeInLeft">
                    <h3 class="section-title">Sobre nosotros</h3>
                    <p class="mt-2">Somos la plataforma líder en respuestas automaticas y gestión para vendedores de
                        Mercado Libre. Día a día, un equipo de más de 10 personas tiene como objetivo ayudarte a vender
                        más rápido y fácil.<br><br>
                        Por eso, estamos convencidos de que detrás de este gran equipo, hay un gran producto para ti.
                    </p>

                    <a class="btn btn-primary mt-40" href="#">Probar Gratis</a>
                </div>
                <div class="col-md-6 wow zoomIn">
                    <div class="image-style-1 d-none d-md-block  ">
                        <img src="assets/img/products/product-img-10.png" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— FEATURES
    ===================================== -->
    <section class="section-padding" id="feature">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-6">
                    <h3 class="section-title-center text-center pt-0 pt-md-5">Herramientas y gestión</h3>
                    <p class="mt-2 text-center">Aprovecha todas sin ningún costo extra, no importa cuál sea tu volumen
                        de ventas.</p>
                </div>
            </div>
            <div class="row mt-70">
                <div class="col-md-6 col-lg-4 wow tafFadeInLeft" data-wow-offset="100">
                    <ul class="nav nav-tabs feature-tabs">
                        <li class="nav-item">
                            <a href="#vtab1" class="nav-link " data-toggle="tab" aria-expanded="true">
                                <div class="media media-style-01 ">
                                    <div class="icon-style-1 order-md-1 custom-ml">
                                        <div class="icon">
                                            <i class="icon_pencil" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body text-md-right">
                                        <h5 class="mt-0">Manage Complex Work</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <div class="media media-style-01">
                                    <div class="icon-style-1 order-md-1 custom-ml">
                                        <div class="icon">
                                            <i class="icon_lightbulb_alt" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body text-md-right">
                                        <h5 class="mt-0">Creative Desing</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab3" class="nav-link" data-toggle="tab">
                                <div class="media media-style-01">
                                    <div class="icon-style-1 order-md-1 custom-ml">
                                        <div class="icon">
                                            <i class="icon_lock_alt" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body text-md-right">
                                        <h5 class="mt-0">Secure Your Data</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab4" class="nav-link" data-toggle="tab">
                                <div class="media media-style-01">
                                    <div class="icon-style-1 order-md-1 custom-ml">
                                        <div class="icon">
                                            <i class="icon_toolbox_alt" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body text-md-right">
                                        <h5 class="mt-0"> unlimited feature</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 d-none d-lg-block overflow">
                    <div class="image-frame" style="background-image: url(assets/img/products/frame2.png)">
                        <div class="tab-content scren-image">
                            <div class="tab-pane fade show no-padding" id="vtab1" aria-expanded="true">
                                <img src="assets/img/products/screen-1.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab2" aria-expanded="true">
                                <img src="assets/img/products/screen-2.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab3" aria-expanded="true">
                                <img src="assets/img/products/screen-3.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab4" aria-expanded="true">
                                <img src="assets/img/products/screen-4.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab5" aria-expanded="true">
                                <img src="assets/img/products/screen-1.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab6" aria-expanded="true">
                                <img src="assets/img/products/screen-2.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab7" aria-expanded="true">
                                <img src="assets/img/products/screen-3.jpg" alt="Image">
                            </div>
                            <div class="tab-pane fade no-padding" id="vtab8" aria-expanded="true">
                                <img src="assets/img/products/screen-1.jpg" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 wow tafFadeInRight" data-wow-offset="100">
                    <ul class="nav nav-tabs feature-tabs">
                        <li class="nav-item">
                            <a href="#vtab5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <div class="media media-style-01 ">
                                    <div class="icon-style-1 custom-mr">
                                        <div class="icon">
                                            <i class="icon_genius" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0">Tight Construction</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab6" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <div class="media media-style-01">
                                    <div class="icon-style-1 custom-mr">
                                        <div class="icon">
                                            <i class="icon_flowchart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0">Flowcharts </h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab7" class="nav-link" data-toggle="tab">
                                <div class="media media-style-01">
                                    <div class="icon-style-1 custom-mr">
                                        <div class="icon">
                                            <i class="icon_cloud-upload_alt" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0">Track Anything</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#vtab8" class="nav-link" data-toggle="tab">
                                <div class="media media-style-01 ">
                                    <div class="icon-style-1 custom-mr">
                                        <div class="icon">
                                            <i class="icon_pencil-edit" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0">Amazingly Easy to Fly</h5>
                                        <p>Aut augue imperdiet pharetra lectus egestas perferendis aliquid nunc.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— WHY OUR CHOOSE
    ===================================== -->
    <section class="bg-gradient section-padding" id="choose-us">
        <div class="container">
            <div class="row">
                <div class="col-md-5 wow tafFlipInY ">
                    <div class="image-style-2 d-none d-md-block ">
                        <img src="assets/img/products/product-img-03.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-12 wow tafFadeInRight">
                    <h3 class="section-title text-white mt-xl-5">¿Por que nosotros?</h3>
                    <p class="mt-2 text-white">En Mercado Libre, una vez que un comprador te realizó una compra, se
                        habilita un chat (también llamado "Mensajería") entre él y tú.
                        <br>
                        <br>
                        Es por esto que te brindamos la posibilidad de activar mensajes automáticos para que sean
                        enviados a tus compradores ante distintas situaciones, y así logres mantenerlos siempre
                        informados, brindándoles la mejor experiencia. Puedes elegir cuáles quieres activar:</p>
                    <ul class="list-style2 mt-3 text-white">
                        <li><i class="arrow_carrot-right"></i>apenas el comprador haya realizado la compra</li>
                        <li><i class="arrow_carrot-right"></i>cuando su producto esté en camino</li>
                        <li><i class="arrow_carrot-right"></i>cuando su producto esté listo para retirarse en sucursal
                        </li>
                        <li><i class="arrow_carrot-right"></i>al ser entregado</li>
                    </ul>
                    <a class="btn btn-outline-white mt-40" href="#">Probar Gratis</a>
                </div>

            </div>
        </div>
    </section>

    <!-- ====================================
    ——— FAQ
    ===================================== -->
    <section class="section-padding bg-whiteSmoke" id="faqs">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-6">
                    <h3 class="section-title-center text-center">Preguntas Frecuentes</h3>
                    <p class="mt-2 text-center">Conoce las preguntas mas frecuentes que nos realizan nuestros clientes,
                        antes de probar nuestro servicio.</p>
                </div>
            </div>
            <div class="row mt-70 justify-content-between">
                <div class="col-12 col-md-6 wow zoomIn">
                    <div class=" d-none d-md-block mt-3">
                        <img class="text-center img-fluid" src="assets/img/products/product-img-05.png" alt="Images">
                    </div>
                </div>
                <div class="col-12 col-md-6 wow tafFadeInRight">
                    <div id="accordion" role="tablist" aria-multiselectable="true" class="mt-0">
                        <div class="card card-accordion">
                            <div class="card-header" role="tab" id="heading1">
                                <h6 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"
                                       aria-expanded="true" aria-controls="collapse1">¿Es Mercado Robot para mi
                                        empresa?</a>
                                </h6>
                            </div>
                            <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1">
                                <div class="card-block">
                                    Si vendes frecuentemente en Mercado Libre, sí.<br><br>
                                    Así tengas menos de diez ventas por mes o seas un gran MercadoLíder o Tienda Oficial
                                    con miles de ventas, todas nuestras herramientas están pensadas para ayudarte a
                                    vender más y mejor.
                                    <br><br>
                                    Miles de vendedores las utilizan a diario y estamos siempre atentos a tus
                                    necesidades a la hora de desarrollar o mejorar nuestras herramientas. Es por esto
                                    que somos la plataforma más utilizada por vendedores de Mercado Libre.

                                </div>
                            </div>
                        </div>
                        <div class="card card-accordion">
                            <div class="card-header" role="tab" id="heading2">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse2" aria-expanded="false" aria-controls="collapse2">¿Es seguro
                                        conectar mi cuenta de Mercado Libre?
                                    </a>
                                </h6>
                            </div>
                            <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="card-block">
                                    ¡Por supuesto! Somos una plataforma certificada por Mercado Libre y la más utilizada
                                    por sus vendedores en múltiples países.
                                    <br><br>
                                    Trabajamos con lo más estrictos estándares de seguridad, para que tu información
                                    esté siempre protegida.
                                </div>
                            </div>
                        </div>
                        <div class="card card-accordion">
                            <div class="card-header" role="tab" id="heading3">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        ¿Cuánto cuesta y cómo abono Mercado Robot?
                                    </a>
                                </h6>
                            </div>
                            <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="card-block">
                                    En Mercado Robot creemos que es algo bueno que siempre tengas acceso a todas
                                    nuestras herramientas, por eso no te cobramos de acuerdo a la cantidad de
                                    herramientas que utilices.
                                    <br><br>
                                    Siempre tienes acceso a todas las herramientas y nuestro abono se paga de forma
                                    mensual o anual. El precio varía de acuerdo con la cantidad de ventas que hayas
                                    tenido en los últimos 30 días. Los precios están disponibles en nuestra sección de
                                    <a href="index.html#price">precios</a>.
                                    <br><br>
                                    Puedes abonar Mercado Robot con cualquiera de los medios de pago disponibles en
                                    MercadoPago: dinero disponible en tu cuenta o tarjeta de crédito.
                                </div>
                            </div>
                        </div>
                        <div class="card card-accordion">
                            <div class="card-header" role="tab" id="heading4">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                        ¿Puedo adherirme a un débito automático?
                                    </a>
                                </h6>
                            </div>
                            <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4">
                                <div class="card-block">

                                    Si optas por pagar de forma mensual, puedes hacerlo pagando un cupón de pago que te
                                    enviamos cada mes, o hacer tu vida más fácil adhiriéndote al débito automático.
                                    <br><br>
                                    Al adherirte, cada mes cobraremos el valor de tu cupón automáticamente del dinero en
                                    cuenta disponible de tu cuenta de MercadoPago.
                                    <br><br>
                                    Si en alguna ocasión no tuvieras dinero en cuenta disponible suficiente para que
                                    cobremos tu mensualidad, automáticamente haremos cuatro reintentos, en el transcurso
                                    de diez días. Si luego de éstos no logramos acreditar tu pago, podrás pagar ese mes
                                    con cualquier otro medio de pago de MercadoPago.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— PRICING TABLE
    ===================================== -->
    <section class="section-padding mb-3" id="price">
        <div class="container ">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-6">
                    <h3 class="section-title-center text-center">Precios</h3>
                    <p class="mt-2 text-center">Nos adaptamos al volumen de tu negocio y queremos acompañarte en tu
                        crecimiento, por eso nuestros precios están pensados para que siempre tengas acceso a todas
                        nuestras herramientas.</p>
                </div>
            </div>
            <div class="row mt-5 align-items-center">
                <div class="col-md-4 wow tafFadeInRight">
                    <div class="card pricing">
                        <div class="card-header">
                            <h3>Junior</h3>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li><span>$116</span></li>
                                <li>0 a 500 ventas</li>
                            </ul>
                            <a href="#" class="btn btn-outline-primary  mt-40" name="login-button" id="login-button">Probar
                                Gratis</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card pricing bg-gradient center ">
                        <div class="card-header">
                            <span>Mas Popular</span>
                            <h3>Senior</h3>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li><span>$290</span></li>
                                <li>501 a 999 ventas</li>
                            </ul>
                            <a href="#" class="btn btn-outline-white mt-40">Probar Gratis</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow tafFadeInLeft">
                    <div class="card pricing ">
                        <div class="card-header">
                            <h3>Zeus</h3>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li><span>$580</span></li>
                                <li>Más de 1.000 ventas</li>
                            </ul>
                            <a href="#" class="btn btn-outline-primary mt-40">Probar Gratis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— PROMO VIDEO
    ===================================== -->
    <section class="section-padding bg-gradient" id="promo">
        <div class="container">
            <div class="row justify-content-center">
                <div class="home-promo-video col-md-6 col-12 ">
                    <div class="promo-video">
                        <a class="video-link" data-fancybox href="https://www.youtube.com/watch?v=PpDBj6V-oII">
                            <span>
                                <i>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         preserveAspectRatio="xMidYMid" width="26" height="34" viewBox="0 0 26 34">
                                      <path d="M25.420,18.349 L2.486,33.849 C2.210,34.037 1.888,34.133 1.567,34.133 C1.305,34.133 1.042,34.071 0.804,33.944 C0.268,33.661 -0.067,33.106 -0.067,32.500 L-0.067,1.499 C-0.067,0.894 0.268,0.338 0.804,0.056 C1.340,-0.229 1.986,-0.189 2.488,0.151 L25.421,15.651 C25.865,15.955 26.133,16.459 26.133,16.999 C26.133,17.540 25.865,18.045 25.420,18.349 Z"
                                            class="video-icon"/>
                                    </svg>
                                </i>

                            </span>
                        </a>
                    </div>
                    <span class="text ">Mira nuestro video</span>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— TEAM MEMBER
    ===================================== -->
    <section class="section-padding">
        <div class="container ">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-6">
                    <h3 class="section-title-center text-center">Equipo de Trabajo</h3>
                    <p class="mt-2 text-center">Somos un equipo de profesionales curiosos, que ejecuta y genera impacto,
                        en busca siempre de los mejores resultados.</p>
                </div>
            </div>
            <div class="team-carouel mt-5">
                <div class="single-item ">
                    <div class="card team">
                        <div class="card-img">
                            <img src="assets/img/team/team-1.jpg" alt="Card image cap">
                            <div class="card-img-overlay">
                                <div class="social-link">
                                    <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_twitter"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_rss"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_vimeo" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="#"><h5>Steven Foster</h5></a>
                            <span>Co-Founder</span>
                        </div>
                    </div>
                </div>
                <div class="single-item team">
                    <div class="card team">
                        <div class="card-img">
                            <img src="assets/img/team/team-2.jpg" alt="Card image cap">
                            <div class="card-img-overlay">
                                <div class="social-link">
                                    <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_twitter"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_rss"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_vimeo" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="#"><h5>Randy Ortan</h5></a>
                            <span>managing director</span>
                        </div>
                    </div>
                </div>
                <div class="single-item team">
                    <div class="card team">
                        <div class="card-img">
                            <img src="assets/img/team/team-3.jpg" alt="Card image cap">
                            <div class="card-img-overlay">
                                <div class="social-link">
                                    <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_twitter"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_rss"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_vimeo" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="#"><h5>Mark Henry</h5></a>
                            <span>Markting Officer</span>
                        </div>
                    </div>
                </div>
                <div class="single-item team">
                    <div class="card team">
                        <div class="card-img">
                            <img class="card-img" src="assets/img/team/team-4.jpg" alt="Card image cap">
                            <div class="card-img-overlay">
                                <div class="social-link">
                                    <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_twitter"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_rss"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_vimeo" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="#"><h5>Katy Perry</h5></a>
                            <span>Web Developer</span>
                        </div>
                    </div>
                </div>
                <div class="single-item team">
                    <div class="card team">
                        <div class="card-img">
                            <img class="card-img" src="assets/img/team/team-2.jpg" alt="Card image cap">
                            <div class="card-img-overlay">
                                <div class="social-link">
                                    <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_twitter"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_rss"
                                                                           aria-hidden="true"></i></a>
                                    <a class="btn-social mr-1" href="#"><i class="social_vimeo" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="#"><h5>Katy Perry</h5></a>
                            <span>Web Developer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— TESTIMONIAL
    ===================================== -->
    <section class="section-padding bg-whiteSmoke" id="customers">
        <div class="container ">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-6">
                    <h3 class="section-title-center text-center">Reseña de clientes</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="testimonial-carousel">
                        <div class="item testimonial-content text-center">
                            <p>Gracias a Mercado Robot, nuestros clientes nos califican positivamente en sus comentarios
                                y reseñas gracias al seguimiento de mensajes automaticos que ofrece la plataforma.</p>
                            <h4>LG MÉXICO / <span> Ventas</span></h4>
                        </div>
                        <div class="item testimonial-content text-center">
                            <p>Mercado Robot es la nueva plataforma que nos permite darle el seguimiento y atención a
                                nuestros clientes con un precio accesible y funcional. </p>
                            <h4>Andre Chakle / <span> CEO</span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ——— CONTACT SECTION
    ===================================== -->
    <section class="section-padding">
        <div class="container ">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="section-title">Contacto</h3>
                    <p class="mt-2"> Si tienes alguna duda de como funciona nuestra plataforma no dudes en ponerte en
                        contacto con nosotros.</p>
                    <div class="contact mt-4">
                        <div class="media mb-3">
                            <i class="icon_pin"></i>
                            <div class="media-body"> Nuevo León 22, 2 Piso, Col. Hipodromo Condesa</div>
                        </div>
                        <div class="media mb-3">
                            <i class="icon_phone"></i>
                            <div class="media-body"> 01800 657 524 332</div>
                        </div>
                        <div class="media mb-3">
                            <i class="icon_mail"></i>
                            <div class="media-body">
                                <a class="text-capitalize"
                                   href="mailTo:hola@mercadorobot.com">hola@mercadorobot.com </a><br>
                            </div>
                        </div>

                    </div>
                    <div class="social-link">
                        <a class="btn-social mr-1" href="#"><i class="social_facebook"></i></a>
                        <a class="btn-social mr-1" href="#"><i class="social_twitter" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <form class="contact-form">
                        <div class="row ">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea class="form-control" rows="6" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-lg btn-primary ml-auto">Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
    ———   FOOTER
    ===================================== -->
    <footer class="footer bg-gradient section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <a class="footer-logo" href="https://www.mercadorobot.com/">
                        <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                             width="200.000000pt" height="100.000000pt" viewBox="0 0 474.000000 100.000000"
                             preserveAspectRatio="xMidYMid meet">
                            <metadata>
                            </metadata>
                            <g transform="translate(0.000000,100.000000) scale(0.100000,-0.100000)"
                               fill="#FFFFFF" stroke="none">
                                <path d="M1011 1461 c-6 -10 -23 -24 -38 -30 -16 -6 -50 -25 -78 -42 -27 -16
-63 -37 -80 -45 -16 -8 -39 -21 -50 -27 -11 -7 -63 -37 -115 -67 -52 -30 -103
-59 -112 -65 -9 -5 -36 -21 -60 -33 -45 -25 -88 -60 -88 -73 0 -12 63 -69 76
-69 7 0 30 -12 51 -26 21 -14 47 -30 58 -35 11 -5 34 -18 50 -28 17 -11 50
-31 75 -44 25 -14 61 -34 80 -45 19 -11 55 -32 80 -45 25 -14 61 -34 80 -45
19 -11 53 -30 75 -42 22 -12 53 -35 69 -51 63 -61 61 -69 58 348 -2 362 -3
379 -23 403 -11 14 -27 37 -37 52 -20 33 -56 37 -71 9z"/>
                                <path d="M1346 1332 c-4 -7 -9 -367 -6 -460 0 -9 16 -12 58 -10 l57 3 3 110
c1 60 6 113 11 118 5 5 20 -12 35 -37 24 -41 31 -46 62 -46 24 0 37 5 41 18
11 29 46 71 55 66 4 -3 8 -57 8 -120 l0 -115 58 3 57 3 0 235 0 235 -51 3
c-59 3 -64 -1 -120 -106 -23 -42 -45 -71 -53 -70 -7 2 -27 28 -43 58 -17 30
-36 62 -43 70 -6 8 -15 23 -18 33 -5 13 -18 17 -56 17 -27 0 -52 -4 -55 -8z"/>
                                <path d="M1916 1321 c-47 -31 -56 -68 -56 -221 0 -218 21 -244 195 -238 l100
3 0 45 0 45 -73 3 c-66 3 -74 5 -84 27 -8 19 -8 31 0 50 10 22 18 24 84 27
l73 3 0 45 0 45 -78 3 c-85 3 -99 13 -79 57 10 22 18 24 84 27 l73 3 0 45 0
45 -105 3 c-90 2 -109 -1 -134 -17z"/>
                                <path d="M2240 1320 c-7 -12 -9 -98 -8 -237 l3 -218 60 0 60 0 3 75 c1 41 5
81 7 88 9 26 40 -6 80 -85 l41 -78 58 -3 c33 -2 63 1 68 6 6 6 1 23 -11 43
-56 96 -75 148 -56 155 54 21 75 164 32 215 -41 49 -75 59 -206 59 -109 0
-121 -2 -131 -20z m218 -100 c12 -12 22 -27 22 -35 0 -24 -51 -56 -84 -53 -28
3 -31 7 -34 42 -2 21 -1 44 2 52 8 21 69 17 94 -6z"/>
                                <path d="M2760 1321 c-71 -38 -100 -102 -100 -221 0 -173 65 -244 218 -238
l77 3 3 54 3 54 -49 -7 c-90 -12 -122 23 -122 134 0 111 32 146 122 134 l49
-7 -3 54 -3 54 -80 2 c-62 2 -88 -2 -115 -16z"/>
                                <path d="M3107 1318 c-23 -22 -32 -50 -72 -238 -8 -36 -20 -85 -26 -110 -6
-25 -12 -58 -13 -75 -1 -29 0 -30 53 -33 62 -4 72 3 86 61 l11 42 54 0 54 0
11 -40 c5 -22 14 -46 18 -53 10 -14 113 -17 122 -3 3 6 -1 41 -10 78 -9 37
-22 97 -30 133 -55 257 -57 260 -165 260 -58 0 -73 -4 -93 -22z m117 -139 c3
-17 6 -49 6 -71 0 -38 -1 -39 -32 -36 -36 3 -36 2 -23 91 5 36 10 47 24 47 13
0 21 -10 25 -31z"/>
                                <path d="M3462 1328 c-17 -17 -17 -439 0 -456 8 -8 50 -12 128 -12 101 1 120
4 154 24 22 12 50 40 62 62 20 34 23 53 23 154 0 101 -3 120 -23 154 -12 22
-40 50 -62 62 -34 20 -53 23 -154 24 -78 0 -120 -4 -128 -12z m199 -98 c31
-17 49 -64 49 -130 0 -89 -32 -140 -89 -140 -32 0 -41 31 -41 140 0 56 5 110
10 121 12 21 41 25 71 9z"/>
                                <path d="M3968 1320 c-20 -11 -47 -36 -60 -57 -21 -35 -23 -49 -23 -163 0
-114 2 -128 24 -163 32 -53 85 -77 165 -77 92 0 138 25 171 95 21 45 25 68 25
145 0 118 -29 187 -91 219 -53 28 -158 28 -211 1z m142 -100 c24 -18 25 -24
25 -120 0 -96 -1 -102 -25 -120 -33 -26 -46 -25 -75 5 -23 22 -25 32 -25 115
0 83 2 93 25 115 29 30 42 31 75 5z"/>
                                <path d="M571 511 c-2 -309 -1 -320 20 -355 23 -40 48 -47 56 -16 3 10 14 22
25 26 11 3 52 25 91 48 40 23 79 46 87 50 8 4 24 12 35 19 11 7 45 26 75 44
67 38 104 76 120 124 11 30 10 40 -4 67 -19 37 -71 80 -141 118 -74 40 -97 53
-130 74 -16 11 -37 23 -45 27 -8 4 -48 26 -88 50 -41 24 -80 43 -87 43 -11 0
-13 -62 -14 -319z"/>
                                <path d="M3900 772 c-6 -2 -10 -12 -10 -23 0 -17 8 -19 74 -19 49 0 77 -4 85
-13 6 -9 12 -84 13 -203 3 -181 4 -189 23 -189 19 0 20 8 23 189 1 119 7 194
13 203 8 9 36 13 86 13 57 0 75 3 80 15 3 9 3 18 1 21 -6 5 -372 11 -388 6z"/>
                                <path d="M1352 548 l3 -223 25 0 c24 0 25 3 28 70 2 38 8 75 14 82 6 8 32 13
64 13 52 0 54 -1 94 -51 22 -28 48 -64 57 -80 18 -31 51 -46 78 -35 12 4 3 22
-42 82 l-57 76 41 38 c70 64 73 160 7 218 l-37 32 -139 0 -138 0 2 -222z m248
167 c17 -9 36 -31 44 -50 13 -30 13 -40 1 -68 -19 -46 -61 -67 -135 -67 -94 0
-100 6 -100 100 0 65 3 81 18 89 27 16 139 13 172 -4z"/>
                                <path d="M2042 740 c-23 -17 -55 -49 -72 -72 -27 -37 -30 -49 -30 -114 0 -110
36 -167 137 -218 44 -22 144 -21 186 2 109 60 131 95 132 212 0 79 -2 88 -30
124 -16 22 -48 52 -69 68 -36 26 -46 28 -126 28 -81 0 -90 -2 -128 -30z m191
-25 c19 -8 47 -28 63 -44 33 -35 59 -123 44 -151 -6 -10 -10 -27 -10 -38 0
-33 -75 -99 -120 -107 -124 -21 -212 52 -212 176 0 80 33 132 104 163 43 19
87 19 131 1z"/>
                                <path d="M2642 548 l3 -223 141 -3 142 -3 43 34 c41 30 44 37 47 84 3 45 0 54
-28 81 -36 36 -37 44 -9 82 31 41 22 113 -17 146 -27 22 -37 23 -177 24 l-147
0 2 -222z m262 171 c43 -20 49 -88 10 -124 -24 -22 -36 -25 -108 -25 -51 0
-87 5 -94 12 -16 16 -16 120 0 136 15 15 158 16 192 1z m29 -212 c20 -17 27
-32 27 -58 0 -57 -25 -72 -129 -77 -120 -6 -131 1 -131 82 0 34 3 66 7 69 3 4
50 7 103 7 86 0 100 -2 123 -23z"/>
                                <path d="M3353 751 c-50 -28 -62 -41 -94 -96 -65 -114 -17 -254 112 -319 56
-29 159 -19 225 22 85 53 125 166 93 263 -14 41 -75 119 -93 119 -8 0 -19 7
-26 15 -19 23 -174 20 -217 -4z m178 -35 c121 -51 146 -208 49 -303 -78 -76
-223 -44 -271 61 -67 148 76 304 222 242z"/>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="col-12">
                    <div class="d-md-block text-center">
                        <a href="#" class="btn-icon mt-4 mt-md-5 mr-3">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="31" height="38" viewBox="0 0 31 38">
                                    <path d="M25.892,20.190 C25.844,15.377 29.824,13.070 30.002,12.955 C27.766,9.689 24.282,9.241 23.041,9.189 C20.076,8.890 17.256,10.932 15.751,10.932 C14.251,10.932 11.929,9.234 9.470,9.278 C6.239,9.326 3.259,11.154 1.595,14.043 C-1.762,19.857 0.735,28.472 4.008,33.188 C5.607,35.496 7.513,38.089 10.017,37.996 C12.427,37.900 13.339,36.439 16.254,36.439 C19.168,36.439 19.987,37.996 22.538,37.949 C25.133,37.900 26.776,35.596 28.364,33.280 C30.200,30.603 30.956,28.009 31.001,27.877 C30.943,27.850 25.942,25.938 25.892,20.190 ZM21.099,6.067 C22.427,4.458 23.324,2.226 23.080,-0.000 C21.166,0.076 18.846,1.272 17.473,2.877 C16.240,4.301 15.162,6.574 15.451,8.756 C17.588,8.923 19.769,7.673 21.099,6.067 Z"
                                          class="brand-icon-svg"/>
                                </svg>

                            </div>
                            <div class="text">
                                <h6>Download it on</h6>
                                <span>App Store</span>
                            </div>
                        </a>
                        <a href="#" class="btn-icon mt-4 mt-md-5 mr-3">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="33" height="37" viewBox="0 0 33 37">
                                    <path d="M30.975,14.967 L6.016,0.534 C5.409,0.184 4.719,-0.001 4.021,-0.001 C3.313,-0.001 2.615,0.189 2.003,0.548 C0.768,1.274 0.000,2.621 0.000,4.066 L0.000,32.932 C0.000,34.377 0.768,35.724 2.003,36.450 C2.615,36.809 3.313,36.999 4.021,36.999 C4.719,36.999 5.409,36.813 6.016,36.463 L30.974,22.029 C32.224,21.308 33.000,19.955 33.000,18.499 C33.000,17.043 32.224,15.690 30.975,14.967 ZM20.159,11.555 L16.732,16.384 L8.535,4.832 L20.159,11.555 ZM5.000,33.999 L16.732,20.613 L20.159,25.443 L5.000,33.999 ZM29.764,19.888 L22.287,24.212 L18.233,18.499 L22.287,12.785 L29.764,17.109 C30.256,17.394 30.562,17.926 30.562,18.499 C30.562,19.072 30.256,19.604 29.764,19.888 Z"
                                          class="brand-icon-svg"/>
                                </svg>

                            </div>
                            <div class="text">
                                <h6>Download it on</h6>
                                <span>App Store</span>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col 12">
                    <p class="text-center text-white mt-70">Copyright © 2018 Mercado Robot S.R.L. de C.V.
                        MercadoRobot.com es una marca registrada. Todos los derechos reservados. </p>
                </div>

            </div>
        </div>
    </footer>
</div>

<!-- ====================================
——— JAVASCRIPT
===================================== -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/slick/slick.min.js"></script>
<script src="assets/plugins/smoothscroll/SmoothScroll.js"></script>
<script src="assets/plugins/velocity/velocity.min.js"></script>
<script src="assets/plugins/wow/wow.min.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.js"></script>
<script src="assets/plugins/waypoint/jquery.waypoints.min.js"></script>
<!-- Custom Js -->
<script src="assets/js/app.js"></script>

<script>
    function login_page(url) {
        location.href = url;
    }
</script>

</body>
</html>

