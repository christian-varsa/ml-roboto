<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . 'libraries/meli.php');

class NotificationController extends CI_Controller
{

    var $meli;
    var $token;
    var $app_id;


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    function __construct()
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->model('CustomMessage');
        $this->load->model('Notification');
        $this->load->database('default');

        $paramsMeli = array('client_id' => CLIENT_ID, 'client_secret' => CLIENT_SECRET, 'access_token' => BETTA_ACCESS_TOKEN);
        $this->load->library('Meli', $paramsMeli);
        $this->meli = new Meli($paramsMeli);


    }

    public function notification()
    {

        $notification = json_decode(file_get_contents('php://input'), TRUE);

        $topic = $notification['topic'];

        $user_token = $this->get_user_token($notification['user_id']);

        print_r($notification['user_id']);

        if ($topic == "orders") {

            $this->token = array('access_token' => $user_token);

            $url = $notification['resource'];
            $this->app_id = $notification['application_id'];

            $resource_id = explode("/orders/", $notification['resource']);


            $response = $this->meli->get($url, $this->token, true);
            $data = $response['body'];

            print_r($data);


            $status = $data['shipping']['status'];
            $id_shipping = $data['shipping']['id'];


            $name = $data['buyer']['first_name'];
            $buyer_id = $data['buyer']['id'];

            $this->send_message($status, $notification['user_id'], $name, $buyer_id, $resource_id[1], $id_shipping);

            if (!$this->Notification->notification_tracking_sended($id_shipping)) {
                $this->send_tracking_notification($status, $notification['user_id'], $name, $buyer_id, $resource_id[1], $id_shipping);
            }


        } else {
            $this->save_undefined_notification($notification, $topic, $notification['user_id']);
        }

        echo json_encode("ok");


    }

    public function get_user_token($userId)
    {
        $token = $this->User->getToken($userId);
        return $token;
    }

    public function save_undefined_notification($notification, $topic, $id_meli)
    {
        $this->Notification->save_undefined_notification($notification, $topic, $id_meli);
    }

    public function send_message($status, $id_meli, $name, $buyer_id, $resource_id, $id_shipping)
    {

        $message = "";

        switch ($status) {
            case "handling":
                $message = $this->CustomMessage->get_messages(MESSAGE_TYPE_ONE, $id_meli);
                if ($message == "") {
                    $message = "@COMPRADOR, su pago ya fue autorizado.";
                }
                $message = str_replace("@COMPRADOR", $name, $message);
                print_r("handling");

                break;
            case "shipped":
                $message = $this->CustomMessage ->get_messages(MESSAGE_TYPE_TWO, $id_meli);
                if ($message == "") {
                    $message = "@COMPRADOR, su pedido ya ha salido de las instalaciones.";
                }
                $message = str_replace("@COMPRADOR", $name, $message);
                print_r("shipped");

                break;
            case "delivered":
                $message = $this->CustomMessage->get_messages(MESSAGE_TYPE_THREE, $id_meli);
                if ($message == "") {
                    $message = "@COMPRADOR, su pedido ha sido entregado.";
                }
                $message = str_replace("@COMPRADOR", $name, $message);
                print_r("delivered");

                break;
        }

        print_r($message);

        $body = array(
            'from' => array('user_id' => $id_meli),
            'to' => array(
                array(
                    'user_id' => $buyer_id,
                    'resource' => 'orders',
                    'resource_id' => $resource_id,
                    'site_id' => SITE_ID,
                )
            ),
            "subject" => "",
            'text' => array('plain' => $message),
        );


        $params = array('access_token' => $this->token['access_token'], 'application_id' => APP_ID);


        $IniMsj = $this->meli->post('/messages', $body, $params);
        print_r($IniMsj);

    }

    function send_tracking_notification($status, $id_meli, $name, $buyer_id, $resource_id, $id_shipping)
    {


        $message = "Hola " . $name . " ¿Cómo estás?<br>" . " Tu pedido ya está en proceso. Puedes ver el estatus " . "<a href='https://myaccount.mercadolibre.com.mx/purchases/shipments/" . $id_shipping . "/detail' target='_blank'>haciendo clic aquí</a>
                (Código de seguimiento: " . $id_shipping . ").<br>Estamos para servirte :)";
        $body = array(
            'from' => array('user_id' => $id_meli),
            'to' => array(
                array(
                    'user_id' => $buyer_id,
                    'resource' => 'orders',
                    'resource_id' => $resource_id,
                    'site_id' => SITE_ID,
                )
            ),
            "subject" => "",
            'text' => array('plain' => $message),
        );


        $params = array('access_token' => $this->token['access_token'], 'application_id' => APP_ID);

        $IniMsj = $this->meli->post('/messages', $body, $params);

        $this->Notification->save_tracking_message($id_shipping);


        print_r($IniMsj);

    }
}