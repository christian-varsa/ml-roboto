<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . 'libraries/meli.php');

/**
 * Created by PhpStorm.
 * User: christian.vargas
 * Date: 10/3/18
 * Time: 11:26 PM
 */
class HomeController extends CI_Controller
{

    var $meli;


    function __construct()
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->database('default');


        $paramsMeli = array('client_id' => CLIENT_ID, 'client_secret' => CLIENT_SECRET, 'access_token' => BETTA_ACCESS_TOKEN);
        $this->load->library('Meli', $paramsMeli);
        $this->meli = new Meli($paramsMeli);

    }

    public function index()
    {
        $this->create_meli_session();
    }

    public function create_meli_session()
    {

        if (isset($_GET['code']) || isset($this->session->access_token)) {
            // If code exist and session is empty
            if (isset($_GET['code']) && !isset($this->session->access_token)) {
                print_r($this->session->access_token);

                // //If the code was in get parameter we authorize
                try {

                    $user = $this->meli->authorize($_GET["code"], base_url(REDIRECT_URI));


                    // Now we create the sessions with the authenticated user
                    $this->session->access_token = $user['body']->access_token;
                    $this->session->expires_in = time() + $user['body']->expires_in;
                    $this->session->refresh_token = $user['body']->refresh_token;
                    $this->session->user_id = $user['body']->user_id;

                } catch (Exception $e) {

                    echo "Exception: ", $e->getMessage(), "\n";
                }
            } else {

                // We can check if the access token in invalid checking the time


                if ($_SESSION['expires_in'] < time()) {

                    try {
                        // Make the refresh proccess
                        $refresh = $this->meli->refreshAccessToken();
                        // Now we create the sessions with the new parameters
                        $this->session->access_token = $refresh['body']->access_token;
                        $this->session->expires_in = time() + $refresh['body']->expires_in;
                        $this->session->refresh_token = $refresh['body']->refresh_token;


                    } catch (Exception $e) {

                        echo "Exception: ", $e->getMessage(), "\n";
                    }
                }
            }

            $this->create_user();

        } else {

            echo '<a href="' . $this->meli->getAuthUrl(base_url(REDIRECT_URI), Meli::$AUTH_URL[SITE_ID]) . '">Login using MercadoLibre oAuth 2.0</a>';
        }


        $this->load->view('home_view');


    }

    public function create_user()
    {
        $params = array('access_token' => $_SESSION['access_token']);
        $result = $this->meli->get('/users/me', $params);
        print_r($result);
        $this->session->id_meli = $result['body']->id;
        $result['token'] = $this->session->access_token;
        $result['token_refresh'] = $this->session->refresh_token;

        $this->User->save_user($result);

        $this->User->save_token($result);
    }

    public function logout()
    {

        $url = "https://www.mercadolibre.com/jms/mlm/lgz/logout?go=https%3A%2F%2Fauth.mercadolibre.com.mx%2Fauthorization%3Fresponse_type%3Dcode%26client_id%".$this->session->id_meli."&platform_id=ml&application_id=".APP_ID;
        $this->session->sess_destroy();
        redirect($url);


    }

}