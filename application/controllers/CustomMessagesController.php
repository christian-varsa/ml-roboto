<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: christian.vargas
 * Date: 10/10/18
 * Time: 10:28 PM
 */
class CustomMessagesController extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    public function __construct()
    {
        parent:: __construct();
        $this->load->helper('form');
        $this->load->model('CustomMessage');
        $this->load->database('default');

    }

    public function index()
    {

        $first_message = $this->CustomMessage->get_messages($this->session->id_meli, MESSAGE_TYPE_ONE);
        $second_message = $this->CustomMessage->get_messages($this->session->id_meli, MESSAGE_TYPE_TWO);
        $third_message = $this->CustomMessage->get_messages($this->session->id_meli, MESSAGE_TYPE_THREE);
        $data['first_message'] = $first_message;
        $data['second_message'] = $second_message;
        $data['third_message'] = $third_message;

        $this->load->view('custom_messages_view', $data);
    }

    public function save_messages()
    {

        $first_message_description = $this->input->post('first_message');
        $first_checked = $this->input->post('first_message_checked') == "on" ? true : false;

        $second_message_description = $this->input->post('second_message');
        $second_checked = $this->input->post('second_message_checked') == "on" ? true : false;

        $third_message_description = $this->input->post('third_message');
        $third_checked = $this->input->post('third_message_checked') == "on" ? true : false;


        $id_meli = $this->session->id_meli;

        $first_message = new CustomMessage();
        $first_message->init($id_meli, $first_message_description, MESSAGE_TYPE_ONE, $first_checked);

        $second_message = new CustomMessage();
        $second_message->init($id_meli, $second_message_description, MESSAGE_TYPE_TWO, $second_checked);

        $third_message = new CustomMessage();
        $third_message->init($id_meli, $third_message_description, MESSAGE_TYPE_THREE, $third_checked);

        $params = array($first_message, $second_message, $third_message);

        $this->CustomMessage->save_messages($params);
        $this->index();


    }

}
