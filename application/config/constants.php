<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
|--------------------------------------------------------------------------
| DEFINE DEBUG/RELEASE MODE
|--------------------------------------------------------------------------
*/
define('IS_DEBUG', true);

/*
|--------------------------------------------------------------------------
| This are constants for ML Library
|--------------------------------------------------------------------------
*/

//site_id => Se obtiene según el país en: https://api.mercadolibre.com/sitesdefined('SITE_ID', 'MLM');
define('SITE_ID', 'MLM');

//Para mas detalles del usuario actual, podriamos consutarlos reemplazando {user_id} por su seller.id en https://api.mercadolibre.com/users/286674561
define('SELLER_ID', '22');


//Por ejemplo: https://api.mercadolibre.com/sites/MLA/search?nickname=TETE2870021 arroja un JSON con el valor seller.id = 202593498
define('USER_UD', '241382639');


//client_id => Se obtiene según el usuario que publica la App en: https://www.mercadopago.com/mlm/account/credentials?type=basic
define('CLIENT_ID', '2976671122073565');

//client_secret => Se obtiene según el usuario que publica la App en: https://www.mercadopago.com/mlm/account/credentials?type=basic
define('CLIENT_SECRET', 'w5q3yFxVMpVsJOGHsvkqOiyzMKVeXEn7');

//https://developers.mercadolibre.com.mx/apps/home
define('SECRET_KEY', 'w5q3yFxVMpVsJOGHsvkqOiyzMKVeXEn7');

//https://developers.mercadolibre.com.mx/apps/home
define('APP_ID', '2976671122073565');

//public_key => Se obtiene según el usuario que publica la App en: https://www.mercadopago.com/mlm/account/credentials
define('PUBLIC_KEY', 'APP_USR-e4e13ba1-7f28-4639-87da-bd959b8aed8a');

//access_token => Se obtiene según el usuario que publica la App en: https://www.mercadopago.com/mlm/account/credentials
define('ACCESS_TOKEN', 'APP_USR-2610823643947416-090521-6caa2a0583bc79cf412c059fef73adc5-286674561');

//beta_public_key => Se obtiene en la sección [ Modo Sandbox ] en: https://www.mercadopago.com/mlm/account/credentials
define('BETA_PUBLIC_KEY', 'TEST-98f8f7bd-ba3d-4943-9510-d41878497bcd');

//beta_access_token => Se obtiene en la sección [ Modo Sandbox ] en: https://www.mercadopago.com/mlm/account/credentials
define('BETTA_ACCESS_TOKEN', 'TEST-2976671122073565-100722-26c484569bd41d22d8843e1b0adb4b0a-133492342');

//authorization_code_uri => URL estática. GET[client_id] corresponde al CLIENT_ID obtenido de: https://www.mercadopago.com/mlm/account/credentials?type=basic
define('AUTHORIZATION_CODE_URI', 'https://auth.mercadolibre.com.mx/authorization?response_type=code&client_id='.APP_ID);

//redirect_uri => https://developers.mercadolibre.com.mx/apps/home

define('REDIRECT_URI', '/HomeController/index') ;

//TG-5bbc30201f4fe20006ec3018-133492342



//CONSTANT FOR DEVELOPMENT
define('MESSAGE_TYPE_ONE', 1) ;
define('MESSAGE_TYPE_TWO', 2) ;
define('MESSAGE_TYPE_THREE', 3) ;



/*
 *
 *
 *
 * {
    "id": 363560534,
    "nickname": "TETE8202525",
    "password": "qatest3301",
    "site_status": "active",
    "email": "test_user_71972495@testuser.com"
}

{
    "id": 363560707,
    "nickname": "TETE5630950",
    "password": "qatest9884",
    "site_status": "active",
    "email": "test_user_45214008@testuser.com"
}
 */