<?php
/**
 * Created by PhpStorm.
 * User: christian.vargas
 * Date: 10/11/18
 * Time: 12:11 AM
 */

class CustomMessage extends CI_Model
{


    var $id_meli;
    var $message;
    var $message_type;
    var $update;

    function __construct()
    {
        parent::__construct();

    }

    function init($id_meli, $message, $message_type, $update)
    {
        $this->id_meli = $id_meli;
        $this->message = $message;
        $this->message_type = $message_type;
        $this->update = $update;
    }

    function save_messages($params)
    {
        $count = count($params);

        for ($i = 0; $i < $count; $i++) {
            if ($params[$i]->update == 1) {
                $id = $this->message_exist($params[$i]->id_meli, $params[$i]->message_type);

                $array = array(
                    'id' => $id,
                    'id_meli' => $params[$i]->id_meli,
                    'message_type' => $params[$i]->message_type,
                    'message' => $params[$i]->message
                );


                $this->db->replace('custom_message', $array);

            }
        }


    }

    public function message_exist($id_meli, $message_type)
    {

        $query = $this->db->get_where('custom_message', array('id_meli' => $id_meli, 'message_type' => $message_type), 1);


        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return null;
        }

    }

    public function get_messages($id_meli, $message_type)
    {
        $query = $this->db->get_where('custom_message', array('id_meli' => $id_meli, 'message_type' => $message_type), 1);

        if ($query->num_rows() > 0) {
            return $query->row()->message;
        } else {
            switch ($message_type) {
                case MESSAGE_TYPE_ONE:
                    return "PRIMER MENSAJE";
                    break;
                case MESSAGE_TYPE_TWO:
                    return "SEGUNDO MENSAJE";
                    break;
                case MESSAGE_TYPE_THREE:
                    return "TERCERO MENSAJE";
                    break;
            }
        }

    }
}