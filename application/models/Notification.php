<?php
/**
 * Created by PhpStorm.
 * User: christian.vargas
 * Date: 10/18/18
 * Time: 1:03 AM
 */


class Notification extends CI_Model
{


    function __construct()
    {
        parent::__construct();

    }

    function save_undefined_notification($notification, $topic, $id_meli)
    {

        print_r($notification);
        print_r($topic);
        print_r($id_meli);

        $array = array(
            'id' => null,
            'id_meli' => $id_meli,
            'notification' => implode("|",$notification),
            'topic' => $topic
        );


        $this->db->replace('undefined_notifications', $array);

    }

    function notification_tracking_sended($id_shipping)
    {
        $query = $this->db->get_where('tracking_notification', array('id_shipping' => $id_shipping), 1);

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function save_tracking_message($id_shipping){
        $array = array(
            'id' => null,
            'id_shipping' => $id_shipping
        );


        $this->db->replace('tracking_notification', $array);
    }
}