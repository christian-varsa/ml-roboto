<?php
/**
 * Created by PhpStorm.
 * User: christian.vargas
 * Date: 10/9/18
 * Time: 1:05 AM
 */

class User extends CI_Model
{


    function __construct()
    {
        parent::__construct();
    }


    function save_user($data)
    {

        $id = $this->user_exist($data['body']->id);

        $array = array(
            'id' => $id,
            'id_meli' => $data['body']->id,
            'nickname' => $data['body']->nickname,
            'first_name' => $data['body']->first_name,
            'lastname' => $data['body']->last_name,
            'email' => $data['body']->email,
            'permalink' => $data['body']->permalink,
            'country_id' => $data['body']->country_id
        );


        $this->db->replace('user', $array);

    }

    function save_token($data)
    {

        $id = $this->token_user_exist($data['body']->id);

        $array = array(
            'id' => $id,
            'id_meli' => $data['body']->id,
            'token' => $data['token'],
            'token_refresh' => $data['token_refresh']
        );


        $this->db->replace('user_token', $array);
    }

    public function user_exist($id)
    {

        $query = $this->db->get_where('user', array('id_meli' => $id), 1);


        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return null;
        }


    }

    public function token_user_exist($id)
    {

        $query = $this->db->get_where('user_token', array('id_meli' => $id), 1);


        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return null;
        }

    }

    public function getToken($data)
    {

        $query = $this->db->get_where('user_token', array('id_meli' => $data), 1);


        if ($query->num_rows() > 0) {
            return $query->row()->token;
        } else {
            return null;
        }
    }

    public function get_user_site($data)
    {

        $query = $this->db->get_where('user', array('id_meli' => $data), 1);


        if ($query->num_rows() > 0) {
            return $query->row()->country_id;
        } else {
            return null;
        }
    }


}