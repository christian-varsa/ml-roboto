-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-10-2018 a las 17:11:30
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ml-robot`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `custom_message`
--

CREATE TABLE `custom_message` (
  `id` int(11) NOT NULL,
  `id_meli` int(11) NOT NULL,
  `message_type` int(11) NOT NULL,
  `message` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `custom_message`
--

INSERT INTO `custom_message` (`id`, `id_meli`, `message_type`, `message`) VALUES
(1, 133492342, 3, 'TERCERO MENSAJE 2, va?'),
(2, 133492342, 2, 'SEGUNDO MENSAJE, ahora si le di check'),
(3, 133492342, 1, 'Hola @Comprador, prueba guardar y recargar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tracking_notification`
--

CREATE TABLE `tracking_notification` (
  `id` int(11) NOT NULL,
  `id_shipping` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tracking_notification`
--

INSERT INTO `tracking_notification` (`id`, `id_shipping`) VALUES
(1, '27716897438');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `undefined_notifications`
--

CREATE TABLE `undefined_notifications` (
  `id` int(11) NOT NULL,
  `id_meli` varchar(50) NOT NULL,
  `notification` varchar(500) NOT NULL,
  `topic` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `undefined_notifications`
--

INSERT INTO `undefined_notifications` (`id`, `id_meli`, `notification`, `topic`) VALUES
(1, '363560534', '/orders/1834087423|363560534|payment|2069392825111111|1|2017-10-09T13:58:23.347Z|2017-10-09T13:58:23.329Z', 'payment'),
(2, '363560534', '/orders/1834087423|363560534|payment|2069392825111111|1|2017-10-09T13:58:23.347Z|2017-10-09T13:58:23.329Z', 'payment');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_meli` int(11) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `permalink` varchar(300) DEFAULT NULL,
  `country_id` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `id_meli`, `nickname`, `first_name`, `lastname`, `email`, `permalink`, `country_id`) VALUES
(1, 133492342, 'CHRISTIANVARGAS038', 'christian', 'vargas', 'lirical_niggar@hotmail.com', 'http://perfil.mercadolibre.com.mx/CHRISTIANVARGAS038', 'MX'),
(133492343, 241382639, 'VACH3287468', 'Christian', 'Vargas', 'christian.vs9821@gmail.com', 'http://perfil.mercadolibre.com.mx/VACH3287468', 'MX'),
(133492345, 363560534, 'TETE8202525', 'Test', 'Test', 'test_user_71972495@testuser.com', 'http://perfil.mercadolibre.com.mx/TETE8202525', 'MX'),
(133492346, 363560707, 'TETE5630950', 'Test', 'Test', 'test_user_45214008@testuser.com', 'http://perfil.mercadolibre.com.mx/TETE5630950', 'MX');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `id_meli` varchar(50) NOT NULL,
  `token` varchar(200) NOT NULL,
  `token_refresh` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_token`
--

INSERT INTO `user_token` (`id`, `id_meli`, `token`, `token_refresh`) VALUES
(1, '133492342', 'APP_USR-2976671122073565-101616-0053c76d392013bc07d155e33d2e5366-133492342', 'TG-5bc6469ac0df9c00060bc28c-133492342'),
(2, '363560534', 'APP_USR-2976671122073565-101919-4f9859ffa3745d842217beaa54a3fda8-363560534', 'TG-5bca6a7cd697550006f3627f-363560534'),
(3, '363560707', 'APP_USR-2976671122073565-101919-9b3eb30b2724968e0325244a4f0b0f62-363560707', 'TG-5bca6aa5c0df9c0006bb62d1-363560707');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `custom_message`
--
ALTER TABLE `custom_message`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tracking_notification`
--
ALTER TABLE `tracking_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `undefined_notifications`
--
ALTER TABLE `undefined_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `custom_message`
--
ALTER TABLE `custom_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tracking_notification`
--
ALTER TABLE `tracking_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `undefined_notifications`
--
ALTER TABLE `undefined_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133492347;

--
-- AUTO_INCREMENT de la tabla `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
